<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('Authenticate_model', 'User');
  }
	public function index()
	{
		show_404();
	}

	public function Admin() {
			$data['error'] = '';
			$data['username'] = '';
		if ($this->input->post()) {
			$user = $this->security->xss_clean($this->input->post());
			if ($user['username'] != '' && $user['username'] != null ){
				if ($user['password'] != '' && $user['password'] != null ) {
					$user['password'] = md5($user['password']);
					$result = $this->User->authenticateAdmin($user);
					if($result) {
						$this->setSession($result);
					} else {
						$data['error'] = 'Username or password incorrect';
					}
				} else {
					$data['error'] = 'Password field is required';
					$data['username'] = $user['username'];
				}
			} else {
				$data['error'] = 'Username field is required';
				$data['username'] = $user['username'];
			}
		}
		$this->load->view('loginLayout', $data);
	}

	private function setSession($data) {
		$userdata = array(
			'username' => $data->username,
			'accessType' => $data->accessType,
			'loggedIn' => true
		);
		$this->session->set_userdata($userdata);
		redirect('dashboard');
	}
}
