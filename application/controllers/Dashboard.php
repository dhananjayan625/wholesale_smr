<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if ($this->session->userdata('loggedIn') != true || $this->session->userdata('accessType') != 'admin')
      show_404();
  }

  private function view($data)
  {
    $this->load->view('partials/header', $data);
    $this->load->view($data['main_content']);
    $this->load->view('partials/footer');
  }

  public function logout()
  {
    $this->session->sess_destroy();
    redirect('authenticate/admin');
  }


  public function do_upload()
  {
    $base64 = $this->input->post('pdf');
    $invoiceNumber = $this->input->post('invoiceNumber');
    $fileName = 'invoice_' . $invoiceNumber . '.pdf';
    $base64 = str_replace('data:application/pdf;base64,', '', $base64);
    $data = base64_decode($base64);
    $pdf = fopen(APPPATH . '../assets/uploads/' . $fileName, 'w');
    fwrite($pdf, $data);
    fclose($pdf);
    $this->load->library('email');
    $this->email->initialize($this->config->item('smtp'));
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");

    //Email content
    $htmlContent = '<h1 style={display: block;font-weight: bold;>Testing the email class.</h1><p>Ignore this email. Let me know if any correction in the pdf format.</p.';
    $this->email->from('ganeshkumar@smrdistributor.com', 'SMR DISTRIBUTOR');
    $this->email->to('smrdistributor2255@gmail.com');
    $this->email->subject('Invoice - '.$invoiceNumber);
    $this->email->attach(APPPATH . '../assets/uploads/' . $fileName);
    $this->email->message('Testing the email class.');
    if ($this->email->send()) {
      echo "Success";
    } else {
      echo "Error";
    }
    echo $this->email->print_debugger();
  }
  public function index()
  {
    $this->load->model('PurchaseBills_model', 'bill');
    $this->load->model('Bill_model', 'collection');
    $this->load->model('PurchaseReturn_model', 'returns');
    $data['total_stack_amount'] = $this->bill->getTotalStackAmount()[0]->totalStackAmount;
    $data['total_collection_pending_amount'] = $this->collection->getTotalCollectionPendingAmount()[0]->totalCollectionPending;
    $data['total_purchase_return_amount'] = $this->returns->getTotalPurchaseReturnAmount()[0]->totalPurchaseReturnAmount;
    $data['main_content'] = 'dashboard';
    $data['title'] = 'Dashboard';
    $this->view($data);
  }
  public function category()
  {
    $this->load->model('Category_model', 'category');
    $this->load->model('Company_model', 'company');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Category name', 'required');
    if ($this->form_validation->run() != FALSE) {
      $this->category->addCategory($this->security->xss_clean($this->input->post()));
    }
    $data['main_content'] = 'category';
    $data['title'] = 'Category';
    $data['cateogries'] = $this->category->getAllCategories();
    $data['companies'] = $this->company->getAllCompanies();
    $this->view($data);
  }
  public function product()
  {
    $this->load->model('Company_model', 'company');
    $data['companies'] = $this->company->getAllCompanies();
    $data['main_content'] = 'products';
    $data['title'] = 'Category';
    $this->view($data);
  }
  public function getAllProducts()
  {
    $this->load->model('Product_model', 'product');
    $list = $this->product->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $stack) {
      $full = json_encode($stack);
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $stack->name;
      $row[] = $stack->company;
      $row[] = $stack->hsnCode;
      $row[] = $stack->mrp;
      $row[] = $stack->containerQuantity;
      $row[] = $stack->basicRate;
      $row[] = $stack->gst;
      $row[] = "<a href='#' class='btn btn-sm btn-info' onClick='editProduct(" . $full . ")'>✏️</a> ";
      // $delete = '| <a class="btn btn-sm btn-danger" onClick="deleteProduct('.$stack->id.')" href="#"><i class="fas fa-trash" aria-hidden="true" /></a>';
      $data[] = $row;
    }


    $json_data = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->product->count_all(),
      "recordsFiltered" => $this->product->count_filtered(),
      "data" => $data,
    );


    echo json_encode($json_data);
  }
  public function addProduct()
  {
    $this->load->model('Product_model', 'product');
    echo json_encode($this->product->addProduct($this->security->xss_clean($this->input->post())));
  }
  public function deleteProduct()
  {
    $this->load->model('Product_model', 'product');
    $this->product->deleteProduct($this->security->xss_clean($this->input->post()));
  }
  public function updateProduct()
  {
    $this->load->model('Product_model', 'product');
    $this->product->update($this->security->xss_clean($this->input->post()));
    echo json_encode($this->input->post());
  }
  public function purchase()
  {
    $this->load->model('Category_model', 'category');
    $this->load->model('Company_model', 'company');
    $data['main_content'] = 'purchase';
    $data['title'] = 'Stack Inword';
    if ($this->input->post()) {
      $this->purchase->addPurchase($this->security->xss_clean($this->input->post()));
    }
    $data['companies'] = $this->company->getAllCompanies();
    $this->view($data);
  }
  public function getProductsByText()
  {
    if ($this->input->post()) {
      $this->load->model('Product_model', 'product');
      $this->product->getAllProductsByText($this->security->xss_clean($this->input->post()));
    }
  }
  public function companies()
  {
    $data['main_content'] = 'companies';
    $data['title'] = 'Companies';
    $this->view($data);
  }

  public function updateCompany()
  {
    $this->load->model('Company_model', 'company');
    echo json_encode($this->company->update($this->security->xss_clean($this->input->post())));
  }
  public function addCompany()
  {
    $this->load->model('Company_model', 'company');
    if ($this->input->post()) {
      $this->company->addCompany($this->security->xss_clean($this->input->post()));
    }
  }
  public function getAllCompanies()
  {
    $this->load->model('Company_model', 'company');
    $list = $this->company->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $stack) {
      $full = json_encode($stack);
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $stack->name;
      $row[] = $stack->gstNo;
      $row[] = $stack->mobile;
      $row[] = $stack->mailId;
      $row[] = "<a href='#' class='btn btn-sm btn-info' onClick='editCompany(" . $full . ")'>✏️</a> ";
      $data[] = $row;
    }


    $json_data = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->company->count_all(),
      "recordsFiltered" => $this->company->count_filtered(),
      "data" => $data,
    );


    echo json_encode($json_data);
  }
  public function billing()
  {
    $data['main_content'] = 'billing';
    $data['title'] = 'Stack Outword';
    $this->load->model('PurchaseBill_model', 'bill');
    $data['billNo'] = $this->bill->generateBillNo();
    $this->view($data);
  }
  public function addPurchaseBill()
  {
    if ($this->input->post()) {
      $this->load->model('PurchaseBill_model', 'bill');
      $data = $this->input->post();
      $pending = 1;
      $bill = array('company' => $data['company'], 'invoiceNumber' => $data['invoiceNumber'], 'invoiceDate' => $data['invoiceDate'], 'amount' => $data['total'], 'status' => 1);
      $billId = $this->bill->addBill($bill);
      $bills = $this->input->post('bills');
      $this->load->model('PurchaseBills_model', 'bills');
      foreach ($bills as $bill) {
        $billsData = array('gst' => $bill['gst'], 'product' => $bill['name'], 'basicRate' => $bill['basicRate'], 'hsnCode' => $bill['hsnCode'], 'productId' => $bill['id'], 'purchasePrice' => $bill['taxedRate'], 'mrp' => $bill['mrp'], 'quantity' => $bill['quantity'], 'stack' => $bill['quantity'], 'free' => $bill['free'], 'freeStack' => $bill['free'], 'company' => $data['company'], 'billId' => $billId, 'status' => 1);
        $this->bills->addBill($billsData);
      }
    }
  }

  public function getStacksDatatable()
  {
    $this->load->model('PurchaseBill_model', 'bills');
    $list = $this->bills->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $stack) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $stack->invoiceNumber;
      $row[] = $stack->product;
      $row[] = $stack->purchasePrice;
      $row[] = $stack->mrp;
      $row[] = $stack->quantity;
      $row[] = $stack->stack;
      $row[] = $stack->free;
      $row[] = $stack->freeStack;
      $row[] = $stack->company;
      $data[] = $row;
    }


    $json_data = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->bills->count_all(),
      "recordsFiltered" => $this->bills->count_filtered(),
      "data" => $data,
    );


    echo json_encode($json_data);
  }

  public function stack()
  {
    $this->load->model('PurchaseBill_model', 'bills');
    $data['main_content'] = 'stack';
    $data['title'] = 'Stack';
    $data['stacks'] = $this->bills->getAllStacks();
    $this->view($data);
  }

  public function getStacksByText()
  {
    if ($this->input->post()) {
      $this->load->model('PurchaseBills_model', 'purchase');
      $this->purchase->getStacksByText($this->security->xss_clean($this->input->post()));
    }
  }

  public function addCustomer()
  {
    $this->load->model('Customers_model', 'customers');
    $this->customers->addCustomer($this->security->xss_clean($this->input->post()));
  }

  public function updateCustomer()
  {
    $this->load->model('Customers_model', 'customers');
    $this->customers->update($this->security->xss_clean($this->input->post()));
  }

  public function customers()
  {
    // $this->load->model('Customers_model', 'customers');
    // $this->load->library('form_validation');
    // $this->form_validation->set_rules('name', 'Name', 'required');
    // $this->form_validation->set_rules('pincode', 'Pincode', 'required');
    // $this->form_validation->set_rules('address', 'Address', 'required');
    // $this->form_validation->set_rules('city', 'City', 'required');
    // $this->form_validation->set_rules('state', 'State', 'required');
    // $this->form_validation->set_rules('mobile', 'Mobile No.', 'required');
    // $this->form_validation->set_rules('mailId', 'E-mail', 'required');
    // if ($this->form_validation->run() != FALSE) {
    //   $this->customers->addCustomer($this->security->xss_clean($this->input->post()));
    // }

    $data['main_content'] = 'customers';
    $data['title'] = 'Customer';
    $this->view($data);
  }

  public function getAllCustomers()
  {
    $this->load->model('Customers_model', 'customers');
    $list = $this->customers->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $customer) {
      $full = json_encode($customer);
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $customer->name;
      $row[] = $customer->gstNo;
      $row[] = $customer->mobile;
      $row[] = $customer->mailId;
      $row[] = "<a href='#' class='btn btn-sm btn-info' onClick='editCustomer(" . $full . ")'>✏️</a> ";
      $data[] = $row;
    }


    $json_data = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->customers->count_all(),
      "recordsFiltered" => $this->customers->count_filtered(),
      "data" => $data,
    );


    echo json_encode($json_data);
  }

  public function getCustomersByText()
  {
    if ($this->input->post()) {
      $this->load->model('Customers_model', 'customers');
      $this->customers->getCustomersByText($this->security->xss_clean($this->input->post()));
    }
  }

  public function addBill()
  {
    if ($this->input->post()) {
      $this->load->model('Bill_model', 'bill');
      $data = $this->input->post();
      $pending = 1;
      $bill = array('customer' => $data['customer'], 'invoiceNumber' => $data['invoiceNumber'], 'invoiceDate' => $data['invoiceDate'], 'amount' => $data['total'], 'status' => 1);
      $billId = $this->bill->addBill($bill);
      $bills = $this->input->post('bills');
      $this->load->model('Bills_model', 'bills');
      $this->load->model('PurchaseBills_model', 'stack');
      foreach ($bills as $bill) {
        $stack = $bill['stack'] - $bill['quantity'];
        $freeStack = $bill['freeStack'] - $bill['free'];
        $update = array('stack' => $stack, 'freeStack' => $freeStack);
        $this->stack->updateStack($update, $bill['id']);
        $billsData = array('product' => $bill['product'], 'hsnCode' => $bill['hsnCode'], 'company' => $bill['company'], 'productId' => $bill['id'], 'purchasePrice' => $bill['taxedRate'], 'mrp' => $bill['mrp'], 'quantity' => $bill['quantity'], 'free' => $bill['free'], 'billId' => $billId, 'status' => 1, 'gst' => $bill['gst'], 'basicRate' => $bill['basicRate']);
        $this->bills->addBill($billsData);
      }
    }
  }

  public function collections()
  {
    if ($this->input->post()) {
      $geted = $this->security->xss_clean($this->input->post('data'));
      $total = $this->security->xss_clean($this->input->post('amount'));
      $this->load->model('Bill_model', 'bill');
      $this->bill->updateAmount(array('paidAmount' => $total), $this->security->xss_clean($this->input->post('invoiceNumber')));
      $this->load->model('Collection_model', 'collection');
      $this->collection->addCollection($geted);
      exit();
    }
    $data['main_content'] = 'collection';
    $data['title'] = 'Collections';
    $this->view($data);
  }

  public function getDetailsOfInvoiceNumber()
  {
    if ($this->input->post()) {
      $this->load->model('Bill_model', 'bill');
      $this->bill->getDetailsOfInvoiceNumber($this->security->xss_clean($this->input->post('invoiceNumber')));
    }
  }

  public function getInvoiceByText()
  {
    if ($this->input->post()) {
      $this->load->model('Bill_model', 'bill');
      $this->bill->getInvoiceByText($this->security->xss_clean($this->input->post()));
    }
  }

  public function getCollectionsByInvoice()
  {
    if ($this->input->post()) {
      $this->load->model('Collection_model', 'collection');
      $this->collection->getCollectionsByInvoice($this->security->xss_clean($this->input->post()));
    }
  }

  public function getPurchaseReturnsByInvoice()
  {
    if ($this->input->post()) {
      $this->load->model('PurchaseReturn_model', 'return');
      $this->return->getPurchaseReturnsByInvoice($this->security->xss_clean($this->input->post()));
    }
  }

  public function getListOfInvoice()
  {
    if ($this->input->post()) {
      $this->load->model('Bill_model', 'bill');
      $this->bill->getListOfInvoice($this->security->xss_clean($this->input->post()));
    }
  }

  public function addPurchaseReturn()
  {
    if ($this->input->post()) {
      $this->load->model('Bill_model', 'bill');
      $this->bill->updateAmount(array('paidAmount' => $this->security->xss_clean($this->input->post('amount'))), $this->security->xss_clean($this->input->post('invoiceNumber')));
      $this->load->model('PurchaseReturn_model', 'return');
      $this->return->addPurchaseReturn($this->security->xss_clean($this->input->post('data')));
    }
  }

  public function invoice()
  {
    $data['main_content'] = 'invoice';
    $data['title'] = 'Invoice';
    $this->load->model('Bill_model', 'bill');
    $data['invoices'] = $this->bill->getAllInvoices();
    $this->view($data);
  }

  public function purchaseReport()
  {
    $data['main_content'] = 'report';
    $data['title'] = 'Report';
    $this->load->model('PurchaseBills_model', 'bills');
    $this->view($data);
  }
  public function invoiceReport()
  {
    $data['main_content'] = 'invoiceReport';
    $data['title'] = 'Report';
    $this->load->model('Bill_model', 'bills');
    $this->view($data);
  }


  public function getReportDatatable()
  {
    $this->load->model('PurchaseBill_model', 'bills');
    $list = $this->bills->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $stack) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $stack->invoiceNumber;
      $row[] = $stack->product;
      $row[] = $stack->purchasePrice;
      $row[] = $stack->mrp;
      $row[] = $stack->quantity;
      $row[] = $stack->stack;
      $row[] = $stack->free;
      $row[] = $stack->freeStack;
      $row[] = $stack->company;
      $row[] = $stack->amount;
      $data[] = $row;
    }


    $json_data = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->bills->count_all(),
      "recordsFiltered" => $this->bills->count_filtered(),
      "data" => $data,
    );


    echo json_encode($json_data);
  }

  public function getInvoiceDatatable()
  {
    $this->load->model('Bill_model', 'bills');
    $list = $this->bills->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $stack) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $stack->invoiceNumber;
      $row[] = $stack->product;
      $row[] = $stack->purchasePrice;
      $row[] = $stack->mrp;
      $row[] = $stack->quantity;
      $row[] = $stack->free;
      $row[] = $stack->company;
      $row[] = $stack->amount;
      $row[] = $stack->paidAmount;
      $data[] = $row;
    }


    $json_data = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->bills->count_all(),
      "recordsFiltered" => $this->bills->count_filtered(),
      "data" => $data,
    );


    echo json_encode($json_data);
  }


  public function purchaseReturns()
  {
    $data['main_content'] = 'purchaseReturns';
    $this->load->model('Company_model', 'company');
    $data['title'] = 'Purchase Return';
    $this->load->model('PurchaseReturn_model', 'returns');
    $data['companies'] = $this->company->getAllCompanies();
    $data['purchaseReturns'] = $this->returns->getAllPurchaseReturns();
    $this->view($data);
  }
  public function checkInvoiceNumber()
  {
      $this->load->model('Bill_model', 'bill');
      $this->bill->existBillId();
  }
  public function getAllInvoices()
  {

    $this->load->model('Invoice_model', 'invoice');
    $list = $this->invoice->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $stack) {
      $full = json_encode($stack);
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $stack->invoiceNumber;
      $row[] = $stack->invoiceDate;
      $row[] = $stack->amount;
      $row[] = $stack->paidAmount;
      $row[] = $stack->amount - $stack->paidAmount;
      $row[] = $stack->name;
      $row[] = "<a target='_blank' href='" . base_url() . "assets/uploads/invoice_" . $stack->invoiceNumber . ".pdf'>View</a>";
      $data[] = $row;
    }


    $json_data = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->invoice->count_all(),
      "recordsFiltered" => $this->invoice->count_filtered(),
      "data" => $data,
    );


    echo json_encode($json_data);
  }
  public function addToStock() {
    if($this->input->post('purchaseId') && $this->input->post('qty') && $this->input->post('id')){
      $quantity = $this->input->post('qty');
      $pId = $this->input->post('purchaseId');
      $id = $this->input->post('id');
      $this->load->model('PurchaseBills_model', 'stack');
        $this->stack->addStock($quantity, $pId, $id);

    }
  }
}
