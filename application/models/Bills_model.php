<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bills_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->table = 'bills';
  }

  public function addBill($data) {
    if($this->db->insert($this->table, $data))
      return true;
  }



  // public function getAllCategories() {
  // 	return $this->db->select('name, id')->where('status', 1)->from($this->table)->get()->result();
  // }

}