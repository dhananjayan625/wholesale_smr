<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collection_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->table = 'collection';
  }

  public function addCollection($data) {
    $wanted=array();
    foreach ($data as $value) {
      $wanted[$value['name']] = $value['value'];
    }
  	if($this->db->insert($this->table, $wanted))
  		return $this->db->insert_id();
  }


  public function getCollectionsByInvoice($text) {
    echo json_encode($this->db->select(' date, type, amount')->where('invoiceNumber', $text['invoiceNumber'])->from($this->table)->get()->result());
  }
  // public function getAllCategories() {
  // 	return $this->db->select('name, id')->where('status', 1)->from($this->table)->get()->result();
  // }

}