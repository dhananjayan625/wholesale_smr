<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->table = 'users';
  }

  public function authenticateAdmin($user) {
    $this->db->select('id, username, accessType');
    $this->db->where('username', $user['username']);
    $this->db->where('password', $user['password']);
    $this->db->where('accessType', 'admin');
    $this->db->from($this->table);
    $query = $this->db->get();
    if($query->num_rows() == 1){
      return $query->row();
    }
    return false;
  }

}