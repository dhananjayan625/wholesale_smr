<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->table = 'customers';
 $this->column_order = array(null, 'name', 'id', 'mobile', 'gstNo', 'pincode', 'address', 'city', 'state', 'mailId');
    $this->column_search = array(null, 'name', 'mobile', 'gstNo', 'pincode', 'address', 'city', 'state', 'mailId');
    $this->order = array('id' => 'DESC');
  }


  private function _get_datatables_query() {
    $this->db->select('name, id, mobile, gstNo, pincode, address, city, state, mailId')->from($this->table);
    $i = 0;
    foreach ($this->column_search as $item) {
      if($i === 0){ $i++; continue; }
      if($_POST['search']['value']) {
        if($i===1) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i) 
          $this->db->group_end();
      }
      $i++;
    }
    $i = 0;
  }
 
  function get_datatables() {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $this->db->where('status', 1);
    if(isset($_POST['order'])) {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if(isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
    $query = $this->db->get();
    return $query->result();
  }
 
  function count_filtered() {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
 
  function count_all() {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
  public function addCustomer($data) {
  	echo $this->db->insert($this->table, $data);
  }

  public function getAllCustomers() {
  	return $this->db->select('name, id, mobile, gstNo, pincode, address, city, state, mailId')->where('status', 1)->order_by('id', 'desc')->from($this->table)->get()->result();
  }

  public function getCustomersByText($text) {
    echo json_encode($this->db->select('name, id, mobile, gstNo, pincode, address, city, state, mailId')->like('name', $text['text'])->from($this->table)->limit(5)->get()->result());
  }

  public function update($data) {
    echo json_encode($data);
    extract($data);
    $this->db->where('id', $id);
    echo $this->db->update($this->table, $data);
  }


}