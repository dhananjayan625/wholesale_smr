<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PurchaseBill_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->table = 'purchasebill';
    $this->column_order = array(null, 'purchasebill.invoiceNumber', 'purchase.product', 'purchase.purchasePrice', 'purchase.mrp', 'purchase.quantity', 'purchase.stack', 'purchase.free', 'purchase.freeStack', 'company.name');
    $this->column_search = array(null, 'purchasebill.invoiceNumber', 'purchase.product', 'purchase.purchasePrice', 'purchase.mrp', 'purchase.quantity', 'purchase.stack', 'purchase.free', 'purchase.freeStack', 'company.name');
    $this->order = array('purchase.id' => 'DESC');
  }

  public function addBill($data) {
  	if($this->db->insert($this->table, $data))
  		return $this->db->insert_id();
  }


  public function getAllStacks() {
    return $this->db->select('purchasebill.invoiceNumber, purchase.product, purchase.purchasePrice, purchase.stack, purchase.free, purchase.freeStack, company.name as company, purchase.mrp, purchase.quantity')->where('purchase.status', 1)->where('purchase.stack >', '0')->or_where('purchase.freeStack >', '0')->from($this->table)->join('purchase', 'purchase.billId = purchasebill.id')->join('company', 'company.id = purchase.company')->get()->result();
  }


  // public function getAllCategories() {
  // 	return $this->db->select('name, id')->where('status', 1)->from($this->table)->get()->result();
  // }

  public function generateBillNo() {
    $result = $this->db->select('invoiceNumber')->from('bill')->order_by('id', 'DESC')->limit(1)->get()->result();
    if($result)
    return $result[0]->invoiceNumber + 1;
    return 850;
  }


  private function _get_datatables_query() {
    $this->db->select('purchasebill.invoiceNumber, purchasebill.amount, purchase.product, purchase.purchasePrice, purchase.stack, purchase.free, purchase.freeStack, company.name as company, purchase.mrp, purchase.quantity')->from($this->table)->join('purchase', 'purchase.billId = purchasebill.id')->join('company', 'company.id = purchase.company');
    $i = 0;
    foreach ($this->column_search as $item) {
      if($i === 0){ $i++; continue; }
      if($_POST['search']['value']) {
        if($i===1) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i) 
          $this->db->group_end();
      }
      $i++;
    }
    $i = 0;
    $temp = 0;
    foreach ($this->column_search as $item) {
      if($i === 0){ $i++; continue; }
      if($_POST['columns'][$i]['search']['value']) {
          $this->db->like($item, $_POST['columns'][$i]['search']['value']);
        // if($temp===0) {
        //   $this->db->group_start();
        //   $this->db->like($item, $_POST['columns'][$i]['search']['value']);
        //   $temp++;
        // } else {
        //   $this->db->or_like($item, $_POST['columns'][$i]['search']['value']);
        // }
        // if(count($this->column_search) - 1 == $i) 
        //   $this->db->group_end();
      }
      $i++;
    }
    // if($temp!==0) 
    //       $this->db->group_end();
  }
 
  function get_datatables() {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $this->db->where('purchase.status', 1);
    if(isset($_POST['order'])) {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if(isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
    if(isset($_POST['startDate']) && isset($_POST['endDate'])) { 
      $this->db->where('purchasebill.invoiceDate >=', $_POST['startDate']);
      $this->db->where('purchasebill.invoiceDate <=', $_POST['endDate']);
    } else {
      $this->db->where('purchase.stack >', '0')->or_where('purchase.freeStack >', '0');
    }
    $query = $this->db->get();
    // echo $this->db->last_query();
    return $query->result();
  }
 
  function count_filtered() {
    $this->_get_datatables_query();
    if(isset($_POST['startDate']) && isset($_POST['endDate'])) { 
      $this->db->where('purchasebill.invoiceDate >=', $_POST['startDate']);
      $this->db->where('purchasebill.invoiceDate <=', $_POST['endDate']);
    } else {
      $this->db->where('purchase.stack >', '0')->or_where('purchase.freeStack >', '0');
    }
    $query = $this->db->get();
    return $query->num_rows();
  }
 
  public function count_all() {
    $this->db->from('purchase');
    return $this->db->count_all_results();
  }



}