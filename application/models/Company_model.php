<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->table = 'company';
    $this->column_order = array(null, 'name', 'gstNo', 'mailId', 'mobile', 'address', 'pincode', 'city', 'state');
    $this->column_search = array(null, 'name', 'gstNo', 'mailId', 'mobile', 'address', 'pincode', 'city', 'state');
    $this->order = array('id' => 'DESC');
  }

  public function addCompany($data) {
  	if($this->db->insert($this->table, $data))
  		return true;
  	return false;
  }

  public function update($data) {
    extract($data);
    $this->db->where('id', $id);
    $this->db->update($this->table, $data);
  }

  public function getAllCompanies() {
  	return $this->db->select('name, id, gstNo, mailId, mobile, address, pincode, city, state')->where('status', 1)->from($this->table)->get()->result();
  }


  private function _get_datatables_query() {
    $this->db->select('name, gstNo, mailId, mobile, address, pincode, city, state, id')->from($this->table);
    $i = 0;
    foreach ($this->column_search as $item) {
      if($i === 0){ $i++; continue; }
      if($_POST['search']['value']) {
        if($i===1) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i) 
          $this->db->group_end();
      }
      $i++;
    }
    // $i = 0;
    // $temp = 0;
    // foreach ($this->column_search as $item) {
    //   if($i === 0){ $i++; continue; }
    //   if($_POST['columns'][$i]['search']['value']) {
    //       $this->db->like($item, $_POST['columns'][$i]['search']['value']);
    //   }
    //   $i++;
    // }
  }
 
  function get_datatables() {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    if(isset($_POST['order'])) {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if(isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
    $query = $this->db->get();
    return $query->result();
  }
 
  function count_filtered() {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
 
  public function count_all() {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

}