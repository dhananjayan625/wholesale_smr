<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->table = 'category';
  }

  public function addCategory($data) {
  	if($this->db->insert($this->table, $data))
  		return true;
  	return false;
  }

  public function getAllCategories() {
  	return $this->db->select('category.name, category.id, company.name as company')->where('category.status', 1)->from($this->table)->join('company', 'company.id = category.company')->get()->result();
  }

}