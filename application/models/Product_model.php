<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->table = 'products';
    $this->column_order = array(null, 'products.name', 'company.name as company', 'products.hsnCode', 'products.mrp', 'products.containerQuantity', 'products.basicRate', 'products.gst', 'products.id');
    $this->column_search = array(null, 'products.name', 'company.name', 'products.hsnCode', 'products.mrp', 'products.containerQuantity', 'products.basicRate', 'products.gst');
    $this->order = array('products.id' => 'DESC');
  }


  public function addProduct($data) {
  	return $this->db->insert($this->table, $data);
  }

  public function update($data) {
    extract($data);
    $this->db->where('id', $id);
    $this->db->update($this->table, $data);
    return true;
  }

  public function deleteProduct($data) {
    $this->db->where('id', $data['id']);
    echo json_encode($this->db->update($this->table, array('status' => 0)));
  }

  public function getAllProducts() {
  	return $this->db->select('products.name, products.mrp, products.gst, products.containerQuantity, products.basicRate, products.hsnCode, company.name as company, products.id')->where('products.status', 1)->from($this->table)->join('company', 'company.id = products.company')->get()->result();
  }

  public function getAllProductsByText($data) {
    echo json_encode($this->db->select('products.name, products.mrp, products.gst, products.containerQuantity, products.basicRate, products.hsnCode, products.id')->where('products.status', 1)->where('company.id', $data['company'])->like('products.name', $data['text'])->from($this->table)->join('company', 'company.id = products.company')->limit(5)->get()->result());
  }

  private function _get_datatables_query() {
    $this->db->select('products.name, products.mrp, products.gst, products.containerQuantity, products.basicRate, products.hsnCode, company.name as company,company.id as companyId, products.retailerMargin, products.id')->from($this->table)->join('company', 'company.id = products.company');
    $i = 0;
    foreach ($this->column_search as $item) {
      if($i === 0){ $i++; continue; }
      if($_POST['search']['value']) {
        if($i===1) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i) 
          $this->db->group_end();
      }
      $i++;
    }
    $i = 0;
    $temp = 0;
    foreach ($this->column_search as $item) {
      if($i === 0){ $i++; continue; }
      if($_POST['columns'][$i]['search']['value']) {
          $this->db->like($item, $_POST['columns'][$i]['search']['value']);
      }
      $i++;
    }
  }
 
  function get_datatables() {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $this->db->where('products.status', 1);
    if(isset($_POST['order'])) {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if(isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
    $query = $this->db->get();
    return $query->result();
  }
 
  function count_filtered() {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
 
  public function count_all() {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }



}