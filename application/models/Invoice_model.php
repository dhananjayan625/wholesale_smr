<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->table = 'bill';
    $this->column_order = array(null, 'bill.invoiceNumber', 'bill.invoiceDate', 'bill.amount', 'bill.paidAmount', 'customers.name');
    $this->column_search = array(null, 'bill.invoiceNumber', 'bill.invoiceDate', 'bill.amount', 'customers.name');
    $this->order = array('bill.id' => 'DESC');
  }


  private function _get_datatables_query() {
    $this->db->select('bill.id, bill.invoiceNumber, bill.invoiceDate, bill.amount, bill.paidAmount, customers.name')->from($this->table)->join('customers', 'customers.id = bill.customer');
    $i = 0;
    foreach ($this->column_search as $item) {
      if($i === 0){ $i++; continue; }
      if($_POST['search']['value']) {
        if($i===1) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i) 
          $this->db->group_end();
      }
      $i++;
    }
    $i = 0;
    $temp = 0;
    foreach ($this->column_search as $item) {
      if($i === 0){ $i++; continue; }
      if($_POST['columns'][$i]['search']['value']) {
          $this->db->like($item, $_POST['columns'][$i]['search']['value']);
      }
      $i++;
    }
  }
 
  function get_datatables() {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $this->db->where('bill.status', 1);
    if(isset($_POST['order'])) {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if(isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
    $query = $this->db->get();
    return $query->result();
  }
 
  function count_filtered() {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
 
  function count_all() {
    $this->db->from('bills');
    return $this->db->count_all_results();
  }

}