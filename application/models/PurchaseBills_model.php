<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PurchaseBills_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->table = 'purchase';
  }

  public function addBill($data) {
  	if($this->db->insert($this->table, $data))
  		return true;
  	return false;
  }


  public function getTotalStackAmount() {
    return $this->db->select('SUM(purchasePrice*stack) as totalStackAmount')->where('stack >', '0')->from($this->table)->get()->result();
  }

  public function getStacksByText($data) {
    echo json_encode($this->db->select('purchase.id, purchase.gst, purchase.product, purchase.purchasePrice, products.retailerMargin, purchase.basicRate, purchase.mrp, purchase.hsnCode, purchase.quantity, purchase.stack, company.id as company, purchase.free, purchase.freeStack')->like('purchase.product', $data['text'])->where('purchase.stack >', '0')->or_where('purchase.freeStack >', '0')->from($this->table)->join('company', 'company.id = purchase.company')->join('products', 'products.id = purchase.productId')->limit(8)->get()->result());
  }

  public function getAllPurchases() {
  	return $this->db->select('products.name as product, purchase,hsnCode, purchase.stack, category.name as category, purchase.id, purchase.free, purchase.freeStack, purchase.quantity, purchase.grade, company.name as company, purchase.purchasePrice, purchase.mrp ')->where('purchase.status', 1)->from($this->table)->join('category', 'category.id = purchase.category')->join('products', 'products.id = purchase.product')->join('company', 'company.id = purchase.company')->get()->result();
  }

  public function updateStack($data, $id) {
    $this->db->where('id', $id)->update($this->table, $data);
  }

  public function getAllInvoicesReport() {
    return $this->db->select('purchasebill.invoiceDate as date,purchase.product, purchase.hsnCode, purchase.mrp, purchase.quantity, purchase.stack, company.name as company ')->from('purchasebill')->join($this->table, 'purchase.billId = purchasebill.id')->join('company', 'company.id = purchase.company')->get()->result();
  }

  public function addStock($qty, $pId, $id)
  {
    $this->db->query('UPDATE purchase SET stack = stack + '.$qty.' where id='.$pId);
    $this->db->query('UPDATE purchaseReturn SET status = 2 WHERE id='.$id);
  }

}