<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bill_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->table = 'bill';
    $this->column_order = array(null, 'bill.invoiceNumber', 'bills.product', 'bills.purchasePrice', 'bills.mrp', 'bills.quantity', 'bills.free', 'company.name');
    $this->column_search = array(null, 'bill.invoiceNumber', 'bills.product', 'bills.purchasePrice', 'bills.mrp', 'bills.quantity', 'bills.free', 'company.name');
    $this->order = array('bills.id' => 'DESC');
  }

  function addBill($data) {
  	if($this->db->insert($this->table, $data))
  		return $this->db->insert_id();
  }

  function updateAmount($data, $id) {
    $this->db->where('invoiceNumber', $id)->update($this->table, $data);
    echo $this->db->last_query();
  }

  function getTotalCollectionPendingAmount() {
    return $this->db->select('SUM(amount-paidAmount) as totalCollectionPending')->from($this->table)->get()->result();
  }

  function getInvoiceByText($text) {
    echo json_encode($this->db->select('invoiceNumber, invoiceDate, id, amount, paidAmount, pending, (amount-paidAmount) as pendingAmount')->like('invoiceNumber', $text['text'])->from($this->table)->limit(5)->get()->result());
  }

  function getDetailsOfInvoiceNumber($invoiceNumber) {
    echo json_encode($this->db->select('invoiceNumber, invoiceDate, id, amount, paidAmount, pending, (amount-paidAmount) as pendingAmount')->where('invoiceNumber', $invoiceNumber)->from($this->table)->get()->result()[0]);
  }


  function getListOfInvoice($text) {
    echo json_encode($this->db->select('bills.product, bills.id, bills.hsnCode, bills.quantity, bills.productId as purchaseId, bills.billId, bills.gst, bills.purchasePrice as taxedRate')->where('bill.invoiceNumber', $text['invoiceNumber'])->like('bills.product', $text['text'])->from($this->table)->join('bills','bills.billId = bill.id')->limit(5)->get()->result());
  }

  function getAllInvoices() {
    return $this->db->select('invoiceNumber, invoiceDate, id, amount, paidAmount, (amount-paidAmount) as pendingAmount')->from($this->table)->get()->result();
  }


  private function _get_datatables_query() {
    $this->db->select('bill.invoiceNumber, bill.amount, bill.paidAmount, bills.product, bills.purchasePrice, bills.free, company.name as company, bills.mrp, bills.quantity')->from($this->table)->join('bills', 'bills.billId = bill.id')->join('company', 'company.id = bills.company');
    $i = 0;
    foreach ($this->column_search as $item) {
      if($i === 0){ $i++; continue; }
      if($_POST['search']['value']) {
        if($i===1) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i) 
          $this->db->group_end();
      }
      $i++;
    }
    $i = 0;
    $temp = 0;
    foreach ($this->column_search as $item) {
      if($i === 0){ $i++; continue; }
      if($_POST['columns'][$i]['search']['value']) {
          $this->db->like($item, $_POST['columns'][$i]['search']['value']);
        // if($temp===0) {
        //   $this->db->group_start();
        //   $this->db->like($item, $_POST['columns'][$i]['search']['value']);
        //   $temp++;
        // } else {
        //   $this->db->or_like($item, $_POST['columns'][$i]['search']['value']);
        // }
        // if(count($this->column_search) - 1 == $i) 
        //   $this->db->group_end();
      }
      $i++;
    }
    // if($temp!==0) 
    //       $this->db->group_end();
  }
 
  function get_datatables() {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $this->db->where('bills.status', 1);
    if(isset($_POST['order'])) {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if(isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
    if(isset($_POST['startDate']) && isset($_POST['endDate'])) { 
      $this->db->where('bill.invoiceDate >=', $_POST['startDate']);
      $this->db->where('bill.invoiceDate <=', $_POST['endDate']);
    }
    $query = $this->db->get();
    // echo $this->db->last_query();
    return $query->result();
  }
 
  function count_filtered() {
    $this->_get_datatables_query();
    if(isset($_POST['startDate']) && isset($_POST['endDate'])) { 
      $this->db->where('bill.invoiceDate >=', $_POST['startDate']);
      $this->db->where('bill.invoiceDate <=', $_POST['endDate']);
    }
    $query = $this->db->get();
    return $query->num_rows();
  }
 
  function count_all() {
    $this->db->from('bills');
    return $this->db->count_all_results();
  }

  function existBillId() {
    $result = $this->db->select('invoiceNumber')->from($this->table)->order_by('id', 'DESC')->limit(1)->get()->result();
    if($result){
    $result = array('status'=> 200, 'invoiceNo' => $result[0]->invoiceNumber + 1);
    echo json_encode($result);
    } else {
      
    $result = array('status'=> 200, 'invoiceNo' => 850);
    echo json_encode($result);
    }

  }

}