<div class="row">
  <div class="form-group col">
    <label for="company">Select company</label>
    <select class="form-control" name="company" id="company" onchange="return filterCompany()">
        <option value="0">Select company...</option>
      <?php foreach ($companies as $company) { ?>
        <option value="<?= $company->id ?>" <?php echo set_select('company', $company->id); ?>><?= $company->name ?></option>
      <?php } ?>
    </select>
  </div>
</div>
<div class="table-responsive mt-5" style="font-size: .9rem;">
  <table class="table table-bordered" id="stackstable" width="100%" cellspacing="0">
    <thead>
      <tr>
        <th>Invoice Number</th>
        <th>Particulr</th>
        <th>Date of returned</th>
        <th>Quantity</th>
        <th>HSN code</th>
        <th>T.Rate</th>
        <th>Amount</th>
        <th>Company</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

      <?php foreach ($purchaseReturns as $return) { ?>
        <tr>
          <td><?= $return->invoiceNumber ?></td>
          <td><?= $return->particular ?></td>
          <td><?= $return->date ?></td>
          <td><?= $return->quantity ?></td>
          <td><?= $return->hsnCode ?></td>
          <td><?= $return->taxedRate ?></td>
          <td><?= $return->amount ?></td>
          <td><?= $return->company ?></td>
          <td><a href="javascript:void(0)" onclick="addToStock(<?= $return->purchaseId ?>, <?= $return->quantity ?>, <?= $return->id ?>)">Add to stock</a></td>
        </tr>
      <?php } ?>
    </tbody>
    <tfoot>
  </table>
</div>
<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script>
  var base_url = $('#base').val(),
    table;
  $(document).ready(function() {

    table = $('#stackstable').DataTable({
      "aLengthMenu": [
        [10, 25, 50, 75, -1],
        [10, 25, 50, 75, "All"]
      ],
      "iDisplayLength": 10,
      "columnDefs": [{
          "targets": [0],
          "orderable": false,
        },

      ],

    });
  });

  const filtetCompany = () => {

  }

  const addToStock = (purchaseId, qty, id) => {

    var request = $.ajax({
      url: base_url + 'dashboard/addToStock',
      type: "POST",
      data: {
        purchaseId,
        qty,
        id
      },
      success: function(response) {
        location.reload();

      }
    });
  }
</script>