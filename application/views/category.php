<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="table-responsive">
      <table class="table table-bordered" id="categoryTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>Category name</th>
            <th>Company name</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>

          <?php $i=1; foreach ($cateogries as $category) { ?>
            <tr>
              <th><?=$i?></th>
              <th><?=$category->company?></th>
              <th><?=$category->name?></th>
              <th><a href="#">Edit</a> | <a href="#">Delete</a></th>
            </tr>
          <?php $i++; } ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-md-4"> 
    <h3 class="py-3">Add category</h3>
    <?php echo validation_errors(); ?>
    <form method="POST" action="">
      <div class="form-group">
        <label for="name">Category name</label>
        <input type="text" value="<?php echo set_value('name'); ?>" class="form-control" id="name" name="name" placeholder="Enter category name">
      </div>
        <div class="form-group">
          <label for="company">Select company</label>
          <select class="form-control" onclick="enableSubmit()" name="company" id="company" onchange="return getProduct()">
            <?php foreach ($companies as $company) {?>
              <option value="<?=$company->id?>" <?php echo set_select('company', $company->id); ?>><?=$company->name?></option>
            <?php } ?>
          </select>
        </div>
      <button type="reset" class="btn btn-secondary">Reset</button>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>