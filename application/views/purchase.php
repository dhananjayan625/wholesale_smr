<style> 
table tbody { border: none; }
</style>
<div class="notPrintDiv">
<div class="row">
  
    <div class=" col-md-3 form-group">
      <label for="company" >Company</label>
      <select class="form-control" name="company" id="company">
        <?php foreach ($companies as $company) {?>
          <option value="<?=$company->id?>"><?=$company->name?></option>
        <?php } ?>
      </select>
    </div>
    <div class="col-md-3 form-group">
      <label for="invoiceNumber">Invoice Number</label>
      <input type="text" name="invoiceNumber" id="invoiceNumber" class="form-control">
    </div>
    <div class="col-md-3 form-group">
      <label for="invoiceDate">Invoice Date</label>
      <input type="date" name="invoiceDate" id="invoiceDate" class="form-control">
    </div>
    <div class="col-md-3">
      <button class="btn btn-success mt-4" id="addToStockBtn" onclick="printBills()"><span>Add to Sock</span>

                    <div class="spinner-border spinner-border-sm ml-2 loading" role="status">
                        <span class="sr-only">Loading...</span>
                    </div></button>
      <button class="btn btn-danger mt-4" id="cancelBills">Clear all</button>
    </div>
</div>
<div class="row">
    <div class="table-responsive">
      <table class="table" style="border: 5px solid thin; " id="categoryTable" width="100%" cellspacing="0">
        <thead class="fixedHeader">
          <tr>
            <th>#</th>
            <th width="300px">Particulars</th>
            <th>HSN Code</th>
            <th>MRP</th>
            <th>B.Rate</th>
            <th>Quantity</th>
            <th>Free</th>
            <th>Disc %</th>
            <th>GST %</th>
            <th>T.Rate</th>
            <th>Amount</th>
            <th colspan="2">Actions</th>
          </tr>
          <tr>
            <th></th>
            <th><input autocomplete="off" type="text" placeholder="Particulars Name" id="product" class="form-control product" name="product"></th>
            <th><input type="number" readonly class="form-control" name="hsnCode" id="hsnCode"></th>
            <th><input type="number" class="form-control" name="mrp" id="mrp"></th>
            <th><input type="number" class="form-control" name="basicRate" id="basicRate"></th>
            <th><input type="number" class="form-control" name="quantity" id="quantity"></th>
            <th><input type="number" class="form-control" name="free" id="free"></th>
            <th><input type="number" class="form-control" name="discount" id="discount" ></th>
            <th><input type="number" class="form-control" readonly="" name="gst" id="gst"></th>
            <th><input type="number" class="form-control" name="taxedRate" id="taxedRate"></th>
            <th id="amount"></th>
            <th><button class="btn btn-success" id="billtick" href="">✓</button></th>
            <th><button class="btn btn-danger" id="billx" href="">X</button></th>
          </tr>
        </thead>
        <tbody id="bills" class="scrollContent">
        </tbody>
        <tfoot>
          <tr>
            <td colspan="8"></td>
            <td>Taxable amount</td>
            <td colspan="3" id="tot"></td>
          </tr>
          <tr>
            <td colspan="8"></td>
            <td>CGST</td>
            <td colspan="3" id="cgst"></td>
          </tr>
          <tr>
            <td colspan="8"></td>
            <td>SGST</td>
            <td colspan="3" id="sgst"></td>
          </tr>
          <tr>
            <td colspan="8"></td>
            <td>Total</td>
            <td colspan="3" id="total"></td>
          </tr>
        </tfoot>
      </table>
    </div>
</div>
</div>
<div id="printdiv">
  <table border="1"  width="100%">
    <thead style="text-align: center;line-height: .8rem !important;" class="font-weight-normal">
      <tr>
        <td colspan="10">
          <h3>Company Name</h3>
          <p>Address of the company</p>
          <p>City, pincode & Contact No.</p>
          <p><b>GST Tin No.: KAFJ234523DSKLJ</b></p>
        </td>
      </tr>
      <tr>
        <td colspan="2">Billing Address</td>
        <td colspan="4">Customer Address</td>
        <td colspan="3" rowspan="2">Invoice No</td>
        <td rowspan="2">Invoice Date</td>
      </tr>
      <tr>
        <td rowspan="2" colspan="2">
          <h5>Company Name</h5>
          <p>Address of the company</p>
          <p>City, pincode & Contact No.</p>
        </td>
        <td colspan="4" rowspan="3">
          <h5><?=$this->config->item('trade_name')?></h5>
          <p><?=$this->config->item('address_of_company')?></p>
          <p><?=$this->config->item('city')?>, <?=$this->config->item('pincode')?> </br>Mobile: <?=$this->config->item('contact_no')?></p>
        </td>
      </tr>
      <tr>
        <td colspan="3" rowspan="2" id="p_invoiceNumber"></td>
        <td rowspan="2" id="p_invoiceDate"></td></tr>
      <tr>
        <td colspan="2"><b>GST Tin No.: <?=$this->config->item('gst_tin_no')?></b></td>
      </tr>
      <tr>
        <th>S. No.</th>
        <th>PARTICULARS</th>
        <th>HSN CODE</th>
        <th>MRP</th>
        <th>QUANT</th>
        <th>B.RATE</th>
        <th>DIS %</th>
        <th>GST %</th>
        <th>T.RATE</th>
        <th>AMOUNT</th>
      </tr>
    </thead>
    <tbody id="printdivBody" class="font-weight-light">
    </tbody>
    <tfoot>
      <tr>
        <td colspan="9">Taxable amount</td>
        <td id="p_taxableAmount"></td>
      </tr>
      <tr>
        <td colspan="7">CGST</td>
        <td colspan="2">9%</td>
        <td id='p_cgst'></td>
      </tr>
      <tr>
        <td colspan="7">SGST</td>
        <td colspan="2">9%</td>
        <td id="p_sgst"></td>
      </tr>
      <tr>
        <th colspan="9">Total</th>
        <th id="p_total"></th>
      </tr>
    </tfoot>
    <!-- <tr>
      <td><b>GST Tin No.:</b></td>
    </tr> -->
  </table>
</div>

<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script>
        $('#addToStockBtn .loading').hide()
  $('#printdiv').hide()
  $('#gst').val(18);
var base_url = $('#base').val();
var purchaseBills = [];
var currentItem = null;
var billsIdCout = 1;
var total = 0;var company = $('#company').val();
var lastTotal = 0;
var invoiceNumber, invoiceDate;
var gstLists = [];
$( document ).ready(function() {
autocomplete({
    input: document.getElementById("product"),
    emptyMsg: "No items found",
    minLength: 3,
    fetch: function(text, update) {
        text = text.toLowerCase();
        // clearCurrentItem();
        var request = $.ajax({
          url: base_url+'dashboard/getProductsByText',
          type: "POST",
          data: {text : text, company: company},
          success: function(response) {
            products = JSON.parse( response );
            let results = []
            products.forEach((product)=> {
              results.push({label: product.name, value: product.id, ...product});
            })
            update(results);
          }
        });
    },
    debounceWaitMs:300,
    enableSubmit:false,
    preventSubmit: false,
    onSelect: function(item) {
      fetchDetails(item);
    }
});
});
function fetchDetails(object) {
  $('#product').val(object.label);
  $('#hsnCode').val(object.hsnCode);
  $('#mrp').val(object.mrp);
  $('#basicRate').val(object.basicRate);
  $('#discount').val(0);
  $('#amount').val(0);
  $('#gst').val(object.gst);
  $('#taxedRate').val(0);
  $('#quantity').val(0);
  $('#free').val(0);
  currentItem = object;
  billValueChangeHandler();
}
function companyChanged() {
  let com = $('#company').val();
  if(company != com){
    if(purchaseBills.length > 0){
      if(confirm('Do you really want to cancel the bills')){
        cancelBills();
        company = com;
        return true;
      } else {
        $('#company').val(com).change();
        return true;
      }
    }
    company = com
    return true;
  }

}
function fetchEditDetails(object) {
  clearCurrentItem();
  $('#product').val(object.label);
  $('#hsnCode').val(object.hsnCode);
  $('#mrp').val(object.mrp);
  $('#basicRate').val(object.basicRate);
  $('#discount').val(object.discount);
  $('#gst').val(object.gst);
  $('#taxedRate').val(object.taxedRate);
  $('#quantity').val(object.quantity);
  $('#free').val(object.free);
  currentItem = object;
  billValueChangeHandler();
  $('#product').focus()
}
$('#gst').keyup(function() {
  billValueChangeHandler();
});
$('#discount').keyup(function() {
  billValueChangeHandler();
});
$('#quantity').keyup(function() {
  billValueChangeHandler();
});
$('#basicRate').keyup(function() {
  billValueChangeHandler();
});
$('#billtick').click(function() {
  billEntry();
})
$('#billx').click(function() {
  billCancel();
})
$('#mdiscount').keyup(function() {
  totalDiscount();
})
$('#tax').keyup(function() {
  totalDiscount();
})
$('#cancelBills').click(function() {
  cancelBills();
})
$('#invoiceDate').change(function() {
  invoiceDate = $('#invoiceDate').val()
})
$('#free').keyup(function() {
  totalDiscount();
})
$('#invoiceNumber').change(function() {
  invoiceNumber = $('#invoiceNumber').val()
})
$('#company').change(function(e) {
  companyChanged();
  e.stopPropagation()
  e.preventDefault()
})
function cancelBills() {
  clearCurrentItem();
  $('#invoiceNumber').val('');
  $('#invoiceDate').val(new Date())
  deleteBill()
  purchaseBills = [];
  gstLists = [];
  billsChangeHandler();
}
function billValueChangeHandler() {
  let quantity = $('#quantity').val();
  let basicRate = $('#basicRate').val();
  let discount = $('#discount').val();
  let gst = $('#gst').val();
  let amount = parseFloat(basicRate)-(parseFloat(basicRate)*(discount/100));
  let taxedRate = parseFloat(amount)+(parseFloat(amount)*(gst/100));
  taxedRate = round(taxedRate, 2)
  amount = parseFloat(quantity) * parseFloat(amount);
  amount = round(amount, 2)
  $('#taxedRate').val(taxedRate);
  $('#amount').empty().text(amount);
  updateCurrentItem();
}
function updateCurrentItem() {
  currentItem.quantity = $('#quantity').val();
  currentItem.basicRate = $('#basicRate').val();
  currentItem.discount = $('#discount').val();
  currentItem.amount = $('#amount').text();
  currentItem.gst = $('#gst').val();
  currentItem.taxedRate = $('#taxedRate').val();
  currentItem.mrp = $('#mrp').val();
  currentItem.free = $('#free').val();
}
function clearCurrentItem() {
  currentItem = null;
  $('#quantity').val('');
  $('#product').val('');
  $('#basicRate').val('');
  $('#discount').val('');
  $('#hsnCode').val('');
  $('#mrp').val('');
  $('#gst').val(0);
  $('#free').val(0);
  $('#taxedRate').val('');
  $('#amount').empty();
  $('#product').focus()
}
function billEntry() {
  if(!currentItem) return false;
  if(currentItem.quantity == 0) 
    if(currentItem.free ==0){
      $('#quantity').focus()
      return false;
    }
  currentItem.unique = billsIdCout;
  billsIdCout++;
  purchaseBills.push(currentItem);
  billsChangeHandler();
  clearCurrentItem();
}
function getBill(unique) {
  object = purchaseBills.filter((bill) => {
    return bill.unique === unique
  })
  return object[0];
}
function billCancel() {
  if(!currentItem) return false;
  clearCurrentItem()
}
function editBill(unique) {
  let editDetails = getBill(unique);
  deleteBill(unique);
  fetchEditDetails(editDetails);
}
function deleteBill(unique) {
  purchaseBills = purchaseBills.filter((bill) => {
    return bill.unique !=unique
  });
  billsChangeHandler();
  $('#product').focus();
}
function billsChangeHandler() {
    $('#bills').empty();
    let i =  1;
    purchaseBills.forEach((data) => {

    $('#bills').append(`<tr><td>${i}</td><td>${data.name}</td><td>${data.hsnCode}</td><td>${data.mrp}</td><td>${data.basicRate}</td><td>${data.quantity}</td><td>${data.free}</td><td>${data.discount}</td><td>${data.gst}</td><td>${data.taxedRate}</td><td>${data.amount}</td><td><button onclick='editBill(${data.unique})' class='btn btn-info'>✏️</button></td><td><button onclick='deleteBill(${data.unique})' class='btn btn-danger'>X</button></td></tr>`);
    i++;
    });
    totalDiscount();
}
function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function groupBy(array, key) {
  return array.reduce((result, obj) => {
     (result[obj[key]] = result[obj[key]] || []).push(obj);
     return result;
  }, {});
}
function totalDiscount() {
    let total = purchaseBills.reduce((currentTotal, bill) => {
      return parseFloat(bill.amount) + parseFloat(currentTotal)
    }, 0)
    // total = round(total, 2);

     let temp = groupBy(purchaseBills, 'hsnCode');
     let keyLists = Object.keys(temp)
     let list=[];
     let gst;
     keyLists.forEach((hsnValue) => {
      let taxableAmount = temp[hsnValue].reduce((currentTotal, bill) => {
        gst = bill.gst
        return parseFloat(bill.amount) + parseFloat(currentTotal)
      }, 0)
      let cgst = taxableAmount * ((gst/2) / 100);
      list.push({
        hsnValue: hsnValue,
        taxableAmount: taxableAmount,
        gst: gst,
        cgst: cgst,
        totalTax: cgst + cgst
      })
      gstLists = list;
     })
     let totaltax = gstLists.reduce((currentTotal, bill) => {
      return parseFloat(bill.totalTax) + parseFloat(currentTotal)
    }, 0)
    let cgst = totaltax / 2;
     lastTotal = total+cgst+cgst;
     lastTotal = Math.floor(lastTotal);
     // temp.forEach((billGroup, hsnValue) => {
     //  let taxableTotal = billGroup.reduce((currentTotal, bill) => {
     //    return parseFloat(bill.amount) + parseFloat(currentTotal)
     //  }, 0)
     //  list.push({
     //    hsnCode: hsnValue,
     //    taxableTotal: taxableTotal,
     //    gst: billGroup[0].gst,
     //  })
     //  console.log(list)
     // })
    $('#cgst').empty().text(cgst);
    $('#tot').empty().text(total);
    $('#sgst').empty().text(cgst);
    $('#total').empty().text(lastTotal);
    $('#p_cgst').empty().text(cgst);
    $('#p_taxableAmount').empty().text(total);
    $('#p_sgst').empty().text(cgst);
    $('#p_total').empty().text(lastTotal);
}
function printBills() {
  if(purchaseBills.length === 0) return false;
  let indate = $('#invoiceDate').val()
  if(indate === null || indate === ''){
    alert('Please fill Invoice Date');
    return false;
  }
  let innum = $('#invoiceNumber').val()
  if(innum === null || innum === ''){
    alert('Please fill Invoice Number');
    return false;
  }
  $('#printdiv').show()
  $('#printdivBody').empty()
  fillPrintContent()
    $('#addToStockBtn').prop('disabled', !0);
        $('#addToStockBtn .loading').show()
      var request = $.ajax({
        url: base_url+'dashboard/addPurchaseBill',
        type: "POST",
        data: {
          bills: purchaseBills,
          total: lastTotal,
          company: company,
          invoiceNumber: innum,
          invoiceDate: indate
        },
        success: function(response) {
          cancelBills();
    $('#addToStockBtn').prop('disabled', 0);
        $('#addToStockBtn .loading').hide()
        },
        error: function (result) {
            //alert(e);
            alert('Please check the network connection')
            // alert(result.status + ' ' + result.statusText);
        }
      });
      $('.notPrintDiv').css("display","none");
      // window.print();
      $('.notPrintDiv').css("display","block");
  $('#printdivBody').empty()
  $('#printdiv').hide()
}
function fillPrintContent() {
    let i =  1;
    $('#p_invoiceNumber').empty().text(invoiceNumber);
    $('#p_invoiceDate').empty().text(invoiceDate);
    purchaseBills.forEach((data) => {

    $('#printdivBody').append(`<tr><td>${i}</td><td>${data.name}</td><td>${data.hsnCode}</td><td>${data.mrp}</td><td>${data.quantity}</td><td>${data.basicRate}</td><td>${data.discount}</td><td>${data.gst}</td><td>${data.taxedRate}</td><td>${data.amount}</td></tr>`);
    i++;
    });
    $('#p_invoiceNumber').empty().text(invoiceNumber);
    $('#p_invoiceDate').empty().text(invoiceDate);
}
</script>