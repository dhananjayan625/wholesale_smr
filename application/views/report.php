<style type="text/css">
  .wid {
    max-width: 5px;
  }
</style>
<div class="row mt-4">
  
    <div class="col-md-4  demo">  
      <lable>Select the date to filter records  
      <input type="text" id="config-demo" class="form-control placeholded">
      </lable>
          
    </div> 
    <div class="col-md-3">
      <h5>Purchase Amount</h5>
      <h5 id="purchaseTotal"></h5>
    </div> 
    <div class="col-md-3">
      <h5>Stack Amount</h5>
      <h5 id="stackTotal"></h5>
    </div> 
    <div class="col-md-3"></div> 
</div>
<div class="row">
    <div class="table-responsive  mt-4">
      <table class="table table-striped" id="ReportTable" width="100%" cellspacing="0">
        <thead>
      <tr>
        <th style="width: 7px;">SI.No</th>
        <th style="width: 7px;" class="th">Invoice Number</th>
        <th class="th">Product Name</th>
        <th style="width: 7px;" class="th">Purchase Price</th>
        <th style="width: 7px;" class="th">MRP</th>
        <th style="width: 7px;" class="th">Quantity</th>
        <th style="width: 7px;" class="th stack">Stock</th>
        <th style="width: 7px;" class="th">Free</th>
        <th style="width: 7px;" class="th stack">Free stock</th>
        <th class="th">Company</th>
        <th style="width: 7px;" class="th">Amount</th>
      </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
          
      <tr>
        <th>SI.No</th>
        <th class="th">Invoice Number</th>
        <th class="th">Product Name</th>
        <th class="th">Purchase Price</th>
        <th class="th">MRP</th>
        <th class="th">Quantity</th>
        <th class="th stack">Stack</th>
        <th class="th">Free</th>
        <th class="th stack">Free Stack</th>
        <th class="th">Company</th>
        <th class="th">Amount</th>
      </tr>
        </tfoot>
      </table>
    </div>
</div>

<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script>
	
var base_url = $('#base').val();
var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var startDate = date, endDate = date, table, totalPurchaseAmount;



function groupBy(array, key) {
  return array.reduce((result, obj) => {
     (result[obj[key]] = result[obj[key]] || []).push(obj);
     return result;
  }, {});
}

$(document).ready( function () {
    $('#ReportTable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
    } );

 
    // DataTable
    table = $('#ReportTable').DataTable({
    // "bPaginate": false,
        "responsive": true,
 
        "processing": true,
        "serverSide": true,
        "order": [],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;
          var intVal = ( i ) => {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
          };
          var stackTotal = api
              .rows(  )
              .data()
              .reduce((a, b) => {
                  return (intVal(b[3]) * intVal(b[6])) + intVal(a);
                }, 0)
              .toFixed(2);
          $('#stackTotal').empty().text(stackTotal)
          let temp =groupBy(api.rows().data(), 1)
          let keyLists = Object.keys(temp)
          let list = []
          keyLists.forEach((invoiceNumber) => {
            list.push(temp[invoiceNumber][0][10])
          })
          
          $('#purchaseTotal').empty().text(list.reduce((a,b) => {
            return parseInt(a) + parseInt(b)
          }, 0).toFixed(2))

        },
        "ajax": {
            "url": base_url+"dashboard/getReportDatatable",
            "type": "POST",
            "data": function ( d ) {
              return $.extend( {}, d, {
                "startDate": startDate,
                "endDate": endDate,
              })
          }
        },
 
        "columnDefs": [
          { 
            "targets": [ 0, 3, 4, 5, 6, 7, 8 ], 
            "orderable": false,
          },
          {
            "targets": [ 10 ],
            "visible":false
          }
        ],
        "initComplete": function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        }

});
    
 
        updateConfig();  
  
        function updateConfig() {  
          var options = {};  
          options.opens = "center";  
options.ranges = {  
              'Today': [moment(), moment()],  
              'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],  
              'Last 7 Days': [moment().subtract(6, 'days'), moment()],  
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],  
              'This Month': [moment().startOf('month'), moment().endOf('month')],  
              'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]  
            };  
          $('#config-demo').daterangepicker(options, function(start, end, label) {   
          startDate = start.format('YYYY-MM-DD'); endDate = end.format('YYYY-MM-DD');  
          table.ajax.reload();     
           });  
            
        }  
  
      });  


  
</script>