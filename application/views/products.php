<div class="row justify-content-center">
  <div class="col-md-12 mt-5">
    <div class="row">
      <div class="h2 col text-uppercase text-black-60">Products details
        <button type="button" class="btn m-2 float-right btn-sm btn-primary btn-labeled" data-toggle="modal" data-target=".bd-example-modal-lg"><span class="btn-label h-100"><i class="fas fa-plus"></i></span>Add Product</button></div>
    </div>
    <div class="table-responsive mt-4 row">
      <table class="table col" id="productTables" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th class="p" style="width: 10px;">#</th>
            <th class="p">Name</th>
            <th class="p">Distributor</th>
            <th class="p">HSN Code</th>
            <th class="p">MRP</th>
            <th class="p">In CTN.</th>
            <th class="p">B.Rate</th>
            <th class="p">GST</th>
            <th class="p" style="width: 80px;">Actions</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
          <tr>
            <th class="p">#</th>
            <th class="p th">Name</th>
            <th class="p th">Distributor</th>
            <th class="p th">HSN Code</th>
            <th class="p th">MRP</th>
            <th class="p th">In CTN.</th>
            <th class="p th">B.Rate</th>
            <th class="p th">GST</th>
            <th class="p">Actions</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <h3 class="py-3 text-center">Add Product</h3>
      <form name="form" class="modal-body" id="addProductForm" method="POST" action="">
        <div class="form-group">
          <label for="name">Product description</label>
          <input autocomplete="off" required type="text" class="form-control" id="name" name="name" placeholder="Enter product description">
        </div>
        <div class="form-group">
          <label for="company">Select distributor</label>
          <select required class="form-control" name="company" id="company">
            <?php foreach ($companies as $company) { ?>
              <option value="<?= $company->id ?>"><?= $company->name ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <div class="col form-group">
            <label for="retailerMargin">Retailer margin in percentage</label>
            <input autocomplete="off" required type="number" class="form-control" id="retailerMargin" name="retailerMargin" placeholder="Enter retailer margin">
          </div>
          <div class="form-group col">
            <label for="containerQuantity">CTN. quantity</label>
            <input type="number" required class="form-control" id="containerQuantity" name="containerQuantity" placeholder="Enter container quantity">
          </div>
        </div>
        <div class="row">
          <div class="form-group col">
            <label for="hsnCode">HSN CODE</label>
            <input autocomplete="off" required type="text" class="form-control" id="hsnCode" name="hsnCode" placeholder="Enter HSN Code">
          </div>
          <div class="form-group col">
            <label for="mrp">MRP</label>
            <input autocomplete="off" required type="number" class="form-control" id="mrp" name="mrp" placeholder="Enter MRP">
          </div>
        </div>
        <div class="row">
          <div class="form-group col">
            <label for="gst">GST</label>
            <input type="number" required class="form-control" id="gst" name="gst" placeholder="Enter GST">
          </div>
          <div class="col form-group">
            <!-- <label for="basicRate">B.Rate</label> -->
            <input autocomplete="off" value="0" required type="hidden" class="form-control" id="basicRate" name="basicRate" placeholder="Enter basic rate">
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-secondary" id="resetbtn" value="Reset">Reset</button>
      </form>
    </div>
  </div>
</div>
<div class="modal fade productEi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <h3 class="py-3 text-center">Edit Product</h3>
      <form name="form" class="modal-body" id="editProductForm" method="POST" action="">
        <input type="hidden" name="eid" id="eid">
        <div class="form-group">
          <label for="name">Product description</label>
          <input autocomplete="off" required type="text" class="form-control" id="ename" name="ename" placeholder="Enter product description">
        </div>
        <div class="form-group">
          <label for="company">Select distributor</label>
          <select required class="form-control" name="ecompany" id="ecompany">
            <?php foreach ($companies as $company) { ?>
              <option value="<?= $company->id ?>"><?= $company->name ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <div class="col form-group">
            <label for="retailerMargin">Retailer margin in percentage</label>
            <input autocomplete="off" required type="text" class="form-control" id="eretailerMargin" name="eretailerMargin" placeholder="Enter retailer margin">
          </div>
          <div class="form-group col">
            <label for="containerQuantity">CTN. quantity</label>
            <input type="text" required class="form-control" id="econtainerQuantity" name="econtainerQuantity" placeholder="Enter container quantity">
          </div>
        </div>
        <div class="row">
          <div class="form-group col">
            <label for="hsnCode">HSN CODE</label>
            <input autocomplete="off" required type="text" class="form-control" id="ehsnCode" name="ehsnCode" placeholder="Enter HSN Code">
          </div>
          <div class="form-group col">
            <label for="mrp">MRP</label>
            <input autocomplete="off" required type="text" class="form-control" id="emrp" name="emrp" placeholder="Enter MRP">
          </div>
        </div>
        <div class="row">
          <div class="form-group col">
            <label for="gst">GST</label>
            <input type="text" required class="form-control" id="egst" name="egst" placeholder="Enter GST">
          </div>
          <div class="col form-group">
            <label for="basicRate">B.Rate</label>
            <input autocomplete="off" required type="text" class="form-control" id="ebasicRate" name="ebasicRate" placeholder="Enter basic rate">
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-secondary" onclick="resetEditProductForm()" id="eresetbtn" value="Reset">Reset</button>
      </form>
    </div>
  </div>
</div>
<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script>
  const base_url = $('#base').val();
  let table;
  $("#resetbtn").click(function() {
    resetAddProductForm()
  });
  $("#editProductForm").submit((e) => {
    e.preventDefault();
    updateProduct();
  });
  $("#addProductForm").submit((e) => {
    e.preventDefault();
    addProduct();
  });
  $(document).ready(function() {
    resetAddProductForm()
    $('#productTables tfoot .th').each(() => {
      const title = $(this).text();
      $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
    });
    table = $('#productTables').DataTable({
      "aLengthMenu": [
        [10, 25, 50, 75, -1],
        [10, 25, 50, 75, "All"]
      ],
      "iDisplayLength": 10,
      "responsive": true,

      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
        "url": base_url + "dashboard/getAllProducts",
        "type": "POST",
      },

      "columnDefs": [{
        "targets": [0, 2, 3, 5, 7, 8],
        "orderable": false,
      }, ],
      "fixedHeader": true,
      "info": false,
      "initComplete": function() {
        // Apply the search
        this.api().columns().every(function() {
          var that = this;

          $('input', this.footer()).on('keyup change clear', function() {
            if (that.search() !== this.value) {
              that
                .search(this.value)
                .draw();
            }
          });
        });
      }


    });
  });
  const resetAddProductForm = () => {
    $('#name').val('');
    $('#company').val('');
    $('#hsnCode').val('');
    $('#mrp').val('');
    $('#gst').val('');
    $('#containerQuantity').val('');
    $('#basicRate').val('');
    $('#retailerMargin').val('');
  }
  const resetEditProductForm = () => {
    $('#ename').val('');
    $('#ecompany').val('');
    $('#ehsnCode').val('');
    $('#emrp').val('');
    $('#egst').val('');
    $('#econtainerQuantity').val('');
    $('#ebasicRate').val('');
    $('#eretailerMargin').val('');
  }
  const fillEditDetails = async ({
    name,
    companyId,
    hsnCode,
    mrp,
    gst,
    containerQuantity,
    basicRate,
    retailerMargin,
    id
  }) => {
    await $('#ename').val(name), $('#ecompany').val(companyId), $('#ehsnCode').val(hsnCode), $('#emrp').val(mrp), $('#egst').val(gst), $('#econtainerQuantity').val(containerQuantity), $('#ebasicRate').val(basicRate), $('#eretailerMargin').val(retailerMargin), $('#eid').val(id);
  }

const convertPercentage = i => i / 100
  const calculateBasicRate = (mrp, retailerMargin, gst) => ((mrp - mrp * convertPercentage(retailerMargin)) / (1 + convertPercentage(gst))).toFixed(2)
  const addProduct = () => {
    let data = {
      name: $('#name').val(),
      company: $('#company').val(),
      hsnCode: $('#hsnCode').val(),
      mrp: $('#mrp').val(),
      gst: $('#gst').val(),
      containerQuantity: $('#containerQuantity').val(),
      retailerMargin: $('#retailerMargin').val(),
      basicRate: calculateBasicRate($('#mrp').val(), $('#retailerMargin').val(), $('#gst').val())
    };
    // data.name = $('#name').val();
    // data.company = $('#company').val();
    // data.hsnCode = $('#hsnCode').val();
    // data.mrp = $('#mrp').val();
    // data.gst = $('#gst').val();
    // data.containerQuantity = $('#containerQuantity').val();
    // data.retailerMargin = $('#retailerMargin').val();
    // data.basicRate = 
    var request = $.ajax({
      url: base_url + 'dashboard/addProduct',
      type: "POST",
      data,
      success: async response => {
        await table.ajax.reload();
        await resetAddProductForm()
        await $('.bd-example-modal-lg').modal('hide');
        alert('Product added successfully')
      }
    });
  }
  const deleteProduct = async id => {
    let isConfirmed = await confirm("Are you sure to delete this product");
    isConfirmed && $.ajax({
      url: base_url + 'dashboard/deleteProduct',
      type: "POST",
      data: {
        id
      },
      success: async response => {
        await table.ajax.reload();
        alert('Product deleted successfully')
      }
    });
  }
  const editProduct = data => {
    console.log('edit product clicked')
    fillEditDetails(data)
    $('.productEi').modal('show')
  }
  const updateProduct = () => {
    let data = {};
    data.id = $('#eid').val();
    data.name = $('#ename').val();
    data.company = $('#ecompany').val();
    data.hsnCode = $('#ehsnCode').val();
    data.mrp = $('#emrp').val();
    data.gst = $('#egst').val();
    data.containerQuantity = $('#econtainerQuantity').val();
    data.basicRate = $('#ebasicRate').val();
    data.retailerMargin = $('#eretailerMargin').val();
    var request = $.ajax({
      url: base_url + 'dashboard/updateProduct',
      type: "POST",
      data,
      success: async response => {
        await table.ajax.reload();
        await resetEditProductForm()
        await $('.productEi').modal('hide');
        alert('Product updated successfully')
      }
    });
  }
</script>