<div class="row mt-4">
	<div class="form-group col-md-4">
		<input type="text" autocomplete="off" name="invoiceNumber" id="invoiceNumber" placeholder="Invoice Number" class="form-control">
	</div>
</div>
<div class="row">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Invoice Number</th>
				<th>Invoice Date</th>
				<th>Amount</th>
				<th>Paid amount</th>
				<th>Pending amount</th>
			</tr>
		</thead>
		<tbody id="invoiceTable"></tbody>
	</table>
</div>
<div class="row mt-4">
  <div class="col-md">
	<h3 class="float-left">Collections</h3>
	<button data-toggle="modal" data-target="#addCollectionModal" class="float-right btn btn-primary" id="addCollectionBtn">Add Collection</button>
  </div>
</div>
<div class="row">
  <table class="table table-bordered">
    <thead>
      <td>#</td>
      <td>Collection date</td>
      <td>Collection type</td>
      <td>Amount</td>
    </thead>
    <tbody id="collectionsTable"></tbody>
  </table>
</div>
<div class="row mt-4">
  <div class="col-md">
  <h3 class="float-left">Purchase return</h3>
  </div>
</div>
<div class="row">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Particular's</th>
        <th>Date</th>
        <th>HSN code</th>
        <th>Qty</th>
        <th>T.Rate</th>
        <th>Amount</th>
      </tr>
      <tr>
        <td><button class="btn btn-primary" id="addPurchaseReturnBtn">Add</button></td>
        <td width="300px"><input type="text" class="form-control" name="returnParticular" id="returnParticular" placeholder="Particular's name"></td>
        <td><input type="date" name="pdate" class="form-control"id="pdate"></td>
        <td id="phsnCode"></td>
        <td><input type="number" style="width: 60px;" name="quantity" class="form-control"id="pquantity"></td>
        <td id="ptaxedRate"></td>
        <td id="pamount"></td>
      </tr>
    </thead>
    <tbody id="PurchaseReturnsTable"></tbody>
  </table>
</div>

    <!-- The Modal -->
<div class="modal" id="addCollectionModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add collection</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form name="addCollection" id="addCollection" method="post" action="">
      <!-- Modal body -->
      <div class="modal-body">
        <input type="hidden" name="invoiceNumber" id="addcollinvoice">
        <div class="col">
        <div class="form-group">
          <label for="date">Collected date</label>
          <input required type="date" name="date" class="form-control" id="date">
        </div>
        <div class="form-group">
          <label for="type">Collection type</label>
          <input required type="text" name="type" class="form-control" id="type" list="typeSuggestion">
          <datalist id="typeSuggestion">
              <option value="cash">
              <option value="cheque">
          </datalist>
        </div>
        <!-- <div class="form-group">
          <label for="type">Collection type</label>
          <select required class="form-control" name="type" id="type">
              <option value="cash">cash</option>
              <option value="cheque" >cheque</option>
          </select>
        </div> -->
        <!-- <div class="form-group">
          <label for="type">Collection Type</label>
          <input required type="t" name="type" class="form-control" id="type">
        </div> -->
        <div class="form-group">
          <label for="amount">Collected amount</label>
          <input required type="number" class="form-control" name="amount" id="amount" placeholder="Collected amount">
        </div>
      </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      <button type="reset" id="addCollectionClear" class="btn btn-secondary">Clear</button>
      <button type="submit" id="addCollectionSubmit" class="btn btn-primary">Submit</button>
      </div>
      
    </form>

    </div>
  </div>
</div> 

  
<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script>
$('#addCollectionBtn').prop('disabled', true);
$('#addPurchaseReturnBtn').prop('disabled', true);
var base_url = $('#base').val();
var purchaseReturn = null;
var invoice, amount = 0, pamount;
$( document ).ready(function() {
autocomplete({
    input: document.getElementById("invoiceNumber"),
    emptyMsg: "No items found",
    minLength: 3,
    fetch: function(text, update) {
        text = text.toLowerCase();
        var request = $.ajax({
          url: base_url+'dashboard/getInvoiceByText',
          type: "POST",
          data: {text : text},
          success: function(response) {
            invoiceNumbers = JSON.parse( response );
            let results = []
            invoiceNumbers.forEach((invoice)=> {
              results.push({label: invoice.invoiceNumber, value: invoice.id, ...invoice});
            })
            update(results);
          }
        });
    },
    debounceWaitMs:300,
    enableSubmit:false,
    preventSubmit: false,
    onSelect: function(item) {
      invoice = item;
      fetchDetails();

    }
});


autocomplete({
    input: document.getElementById("returnParticular"),
    emptyMsg: "No items found",
    minLength: 2,
    fetch: function(text, update) {
        text = text.toLowerCase();
        var request = $.ajax({
          url: base_url+'dashboard/getListOfInvoice',
          type: "POST",
          data: {
            invoiceNumber: invoice.invoiceNumber,
            text : text
          },
          success: function(response) {
            invoiceNumbers = JSON.parse( response );
            let results = []
            invoiceNumbers.map((invoice)=> {
              results.push({label: invoice.product, value: invoice.id, ...invoice});
            })
            update(results);
          }
        });
    },
    debounceWaitMs:300,
    enableSubmit:false,
    preventSubmit: false,
    onSelect: function(item) {
      $('#returnParticular').val(item.product)
      $('#phsnCode').empty().text(item.hsnCode)
      $('#ptaxedRate').empty().text(item.taxedRate)
      $('#pamount').val(0)
      $('#pquantity').val(0)
      $('#pamount').empty().text(0)
      purchaseReturn = item

    }
});
});
$('#pquantity').keyup(function() {
  calculateTotal();
  if($('#pquantity').val() > purchaseReturn.quantity){
    alert('quantity must not greater than '+ purchaseReturn.quantity)
      $('#pquantity').val(0)
  }
})
function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}
function calculateTotal() {
  $('#pamount').empty().text(round($('#pquantity').val() * purchaseReturn.taxedRate, 0))
}
function RefreshInvoiceDetails() {
    var request = $.ajax({
      url: base_url+'dashboard/getDetailsOfInvoiceNumber',
      type: "POST",
      data: {
        invoiceNumber: invoice.invoiceNumber
      },
      success: function(response) {
        invoice = JSON.parse( response );
        fetchDetails()
      }
    });
}
function fetchDetails() {
	$('#addcollinvoice').val(invoice.invoiceNumber)
    $('#invoiceNumber').val(invoice.invoiceNumber);
    // let pp = (invoice.amount === invoice.paidAmount) ? 'Paid' : 'Pending';
    amount = invoice.paidAmount
    $('#invoiceTable').empty().append(`<tr><td>${invoice.invoiceNumber}</td><td>${invoice.invoiceDate}</td><td>${invoice.amount}</td><td id='paidAmount'>${invoice.paidAmount}</td><td>${invoice.pendingAmount}</td></tr>`)
    fetchCollections();
    $('#amount').attr('max', invoice.pendingAmount)
  $('#addCollectionBtn').prop('disabled', false);
  $('#addPurchaseReturnBtn').prop('disabled', false);
}
$( "#addCollection" ).on( "submit", function( event ) {
  event.preventDefault();

$('#addCollectionSubmit').prop('disabled', true);
        amount= parseInt(amount)+parseInt($('#amount').val())
    var request = $.ajax({
      url: base_url+'dashboard/collections',
      type: "POST",
      data: {
      	data: $( this ).serializeArray(),
      	amount: amount,
      	invoiceNumber: invoice.invoiceNumber
      },
      success: function(response) {
      	$('#addCollectionModal').modal('toggle');
$('#addCollectionSubmit').prop('disabled', false);
       //  amount = parseInt(amount)+parseInt($('#amount').val());
      	// $('#paidAmount').empty().text(amount);
        RefreshInvoiceDetails()
      	$('#addCollectionClear').click();
        fetchCollections()
      }
    });
});
$('#addPurchaseReturnBtn').click(function() {
  $('#addPurchaseReturnBtn').prop('disabled', true);
  let quant = parseInt($('#pquantity').val())
  let amt = parseInt($('#pamount').val())
  // if(quant == 0 || amt == 0) return false
  if(($('#pdate').val() === '' || $('#pdate').val() === null)) {
    alert('Please fill date')
  $('#addPurchaseReturnBtn').prop('disabled', false);
    return false
  }
  if(quant <= 0) {
    alert('Quantity must greater than 0')
  $('#addPurchaseReturnBtn').prop('disabled', false);
    return false
  }
  console.log('after validate')
amount= parseFloat(amount)+parseFloat($('#pamount').text())
    var request = $.ajax({
      url: base_url+'dashboard/addPurchaseReturn',
      type: "POST",
      data: {
        data: {
          date: $('#pdate').val(),
          amount: $('#pamount').text(),
          invoiceNumber: invoice.invoiceNumber,
          particular: $('#returnParticular').val(),
          billsId: purchaseReturn.billsId,
          purchaseId: purchaseReturn.purchaseId,
          quantity: $('#pquantity').val(),
          hsnCode: $('#phsnCode').text(),
          taxedRate: $('#ptaxedRate').text(),
        },
        amount: amount,
        invoiceNumber: invoice.invoiceNumber
      },
      success: function(response) {
        // amount = parseInt(amount)+parseInt($('#pamount').val());
        // $('#paidAmount').empty().text(amount);
        RefreshInvoiceDetails()
        purchaseReturn = null
        fetchPurchaseReturns()
  $('#addPurchaseReturnBtn').prop('disabled', false);
      }
    });
})
function fetchCollections(){
    var request = $.ajax({
      url: base_url+'dashboard/getCollectionsByInvoice',
      type: "POST",
      data: {invoiceNumber : invoice.invoiceNumber},
      success: function(response) {
        collections = JSON.parse( response );
        $('#collectionsTable').empty();
        let i = 1;
        collections.forEach((data)=> {
    	  $('#collectionsTable').append(`<tr><td>${i}</td><td>${data.date}</td><td>${data.type}</td><td>${data.amount}</td></tr>`)
    	  i++;
        })
        fetchPurchaseReturns();
      }
    });
}

function fetchPurchaseReturns() {
    var request = $.ajax({
      url: base_url+'dashboard/getPurchaseReturnsByInvoice',
      type: "POST",
      data: {invoiceNumber : invoice.invoiceNumber},
      success: function(response) {
        purchaseReturns = JSON.parse( response );
        $('#PurchaseReturnsTable').empty();
        let i = 1;
        purchaseReturns.forEach((data)=> {
        $('#PurchaseReturnsTable').append(`<tr><td>${i}</td><td>${data.particular}</td><td>${data.date}</td><td>${data.hsnCode}</td><td>${data.quantity}</td><td>${data.taxedRate}</td><td>${data.amount}</td></tr>`)
        i++;
        })
      $('#returnParticular').val('')
      $('#phsnCode').empty().text()
      $('#ptaxedRate').empty().text()
      $('#pamount').val(0)
      $('#pquantity').val(0)
      $('#pamount').empty().text(0)
      }
    });
}
</script>