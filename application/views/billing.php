<style>
    /* table tbody {
    border-collapse: collapse;
  } */

    /*#categoryTable table thead tr td { padding: 0!important; }*/
    /* tbody td {
    border-right: 1px solid;
    border-bottom: 1px solid white;
  }

  .gstListsBody td {
    border-right: 1px solid;
    border-bottom: 1px solid white;
  }

  tfoot td {
    border-top: 1px solid;
  }

  .printdivBody>tr td.next~td {
    text-align: right; */
    /* } */
    /*   table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
*/
    table.report-container {
        page-break-after: always;
    }

    thead.report-header {
        display: table-header-group;
    }

    .blur {
        /*z-index:9999;
    display: none;
    width:100%;
    background-color: rgba(0,0,0,0.5);
    position: absolute;*/
        filter: blur(4px);
        position: absolute;
        width: 100%;
        height: 100%;
    }
</style>
<!-- <link href="https://fonts.googleapis.com/css2?family=Arvo:wght@700&display=swap" rel="stylesheet"> -->
<link href="https://fonts.googleapis.com/css2?family=Arvo&display=swap" rel="stylesheet">
<div class="container-fluid">
    <div class="row">

        <div class="col-md-3 form-group">
            <label for="customerDetails">Customer Name</label>
            <input autocomplete="off" type="text" name="customerDetails" id="customerDetails" class="form-control">
        </div>
        <!-- <div class="col-md-3 form-group">
            <label for="invoiceNumber">Invoice Number</label>
            <input type="text" name="invoiceNumber" id="invoiceNumber" value="<?=$billNo?>" class="form-control">
        </div> -->
        <div class="col-md-3 form-group">
            <label for="invoiceDate">Invoice Date</label>
            <input type="date" name="invoiceDate" id="invoiceDate" class="form-control">
        </div>
        <div class="col-md-3">
            <button class="btn btn-success mt-4" id="printBtn"><span>Print</span>
                <div class="spinner-border spinner-border-sm ml-2 loading" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </button>
            <button class="btn btn-danger mt-4" id="clearAllBtn">Clear all</button>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="tble" id="BillingTable" width="100%" style="font-family: 'Arvo', serif;
">
                <thead class="fixedHeader">
                    <tr>
                        <th style="width: 3vw;">#</th>
                        <th style="width: 30vw;">Particulars</th>
                        <th style="width: 7vw;">HSN Code</th>
                        <th style="width: 5vw;">MRP</th>
                        <th style="width: 5vw;">B.Rate</th>
                        <th style="width: 8vw;">QTY</th>
                        <th style="width: 8vw;">Free</th>
                        <th style="width: 8vw;">Disc</th>
                        <th style="width: 8vw;">R.Mar</th>
                        <th style="width: 5vw;">GST</th>
                        <th style="width: 5vw;">T.Rate</th>
                        <th style="width: 8vw;">Amount</th>
                        <th style="width: 20vw;" colspan="2">Actions</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th><input type="text" placeholder="Particulars Name" autocomplete="off" id="product" class="form-control product" name="product"></th>
                        <th><span class="" id="hsnCode"></span></th>
                        <th><span class="" id="mrp"></span></th>
                        <th><span class="" id="basicRate"></span></th>
                        <th><input type="number" class="form-control" value="0" name="quantity" id="quantity"></th>
                        <th><input type="number" class="form-control" value="0" name="free" id="free"></th>
                        <th><input type="number" class="form-control" value="0" name="discount" id="discount"></th>
                        <th><input type="number" class="form-control" name="retailerMargin" id="retailerMargin"></th>
                        <th><span class="" id="gst"></span></th>
                        <th><span class="" id="taxedRate"></span></th>
                        <th><span class="" id="amount"></span></th>
                        <th><button class="btn btn-success" id="billtick" href="">✓</button></th>
                        <th><button class="btn btn-danger" id="billx" href="">X</button></th>
                    </tr>
                </thead>
                <tbody class="scrollContent allItems" id="allItems">
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="9" rowspan="5">



                            <table align="center" class="mt-5" border="1" width="90%" cellpadding="5">
                                <thead style="text-align: center;line-height: .8rem !important;">
                                    <tr>
                                        <th rowspan="2">GST</th>
                                        <th rowspan="2">Taxable Value</th>
                                        <th colspan="2">Central Tax</th>
                                        <th colspan="2">State Tax</th>
                                        <th rowspan="2">Total Tax Amount</th>
                                    </tr>
                                    <tr>
                                        <th>Rate</th>
                                        <th>Amount</th>
                                        <th>Rate</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody class="gstLists">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Total</th>
                                        <th class="p_taxableAmount text-right"></th>
                                        <th></th>
                                        <th class="p_sgst text-right"></th>
                                        <th></th>
                                        <th class="p_sgst text-right"></th>
                                        <th class="p_totalTAx text-right"></th>
                                    </tr>
                                </tfoot>
                                <tfoot></tfoot>
                            </table>


                        </td>
                        <td colspan="2">Taxable amount</td>
                        <td><span class="float-right taxableAmount"></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">CGST</td>
                        <td><span class="float-right cgst"></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">SGST</td>
                        <td><span class="float-right sgst"></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">ROUNDING OFF</td>
                        <td><span class="float-right rounding"></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">Total</td>
                        <td><span class="float-right total"></span></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<hr />
<div id="check" style="width: 794px;">
    <!-- <div class="text-center font-weight-bold">Tax Invoice</div> -->
    <!--   <div class="head text-center">
            <img class="float-left p-3" src="<?= base_url() ?>/assets/images/vinayagar1.svg">
            <span style="font-weight: bold; font-size: 1em;"></span><br>
            <span><?= $this->config->item('address_of_company') ?></span><br>
            <span><?= $this->config->item('city') ?>, <?= $this->config->item('pincode') ?></span><br>
            <span>Email: smrdistributor2255@gmail.com</span>
        </div> -->
    <style type="text/css">
        /*table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }*/

        @media {
            table.report-container {
                page-break-after: always;
            }

            thead.report-header {
                display: table-header-group;
            }
        }

        /*tfoot.report-footer {
    display:table-footer-group;
}*/
    </style>
    <!-- <div class="row">
            <div class="col-4 align-left text-left float-left text-dark">
                <h5 class="mb-0 font-weight-bolder"><img class="p-3" src="<?= base_url() ?>/assets/images/vinayagar1.svg"><?= $this->config->item('trade_name') ?></h5>
                <div><p class="small mb-0 text-text-uppercase"><?= $this->config->item('address_of_company') ?>,<br/> <?= $this->config->item('city') ?>, <?= $this->config->item('pincode') ?></p></div>
                <div><p class="small mt-0 text-text-uppercase">Email: smrdistributor6522@gmail.com,<br/> Mobile: 1234567896, 7894567899</p></div>
            </div>
            <div class=""><br><br><br>
                <div class="h6 mb-0">Buyer Details</div>
                <div class="small">VASANTHAM SUPER MARKET(MDU)</div>
                <div class="small">Stree of the buyer, mobile , district, etc..,</div>
                <div class="small">GST Tin NO.: KKJASD9FA9FU43009340J40909</div>
            </div>
           <!--  <div class="col">
            <div>Tax Invoice</div><br>
            <div>Invoice No.: 9989</div>
            <br><div>Invoice Date: 29/09/1999</div></div> -->
    <!-- <div class="col align-right text-right float-right">
            <br>
            <div class="h4">Tax Invoice</div>
            <div style="font-size: .9em">Invoice No.: 8989</div>
            <div style="font-size: .9em">Invoice Date: 29/09/1999</div>
            </div>
 </div>  -->
    <!--         <div class="row">
            <div class="col-4">
                <div class="h6 mb-0">Buyer Details</div>
                <div style="font-size: .9em;">VASANTHAM SUPER MARKET(MDU)</div>
                <div style="font-size: .9em;">Stree of the buyer, mobile , district, etc..,</div>
                <div style="font-size: .9em;">GST Tin NO.: KKJASD9FA9FU43009340J40909</div>
            </div>
            <div class="col-4"></div>
            <div class="col-4"></div>
        </div>
        <!-- style="background-color: #ffffff; filter: alpha(opacity=40); opacity: 0.95;border:1px solid;" -->
    <table class="border border-dark" id="BillingTable" width="100%" style="text-align: center;font-size: .7rem; font-family: 'Arvo', serif;
">
        <thead class="fixedHeader report-header">
            <tr class="border-bottom border-dark">
                <td colspan="11">

                    <h5 class="mb-0 font-weight-bolder"><img class="p-3" src="<?= base_url() ?>/assets/images/vinayagar1.svg"><?= $this->config->item('trade_name') ?></h5>
                </td>
            </tr>
            <tr class="border-bottom border-dark">
                <td colspan="11">


                    <div class="row">

                        <div class="col-4 align-left text-left float-left text-dark border-right border-dark ml-1">
                            <div>
                                <div class="h6 mb-0">Billing Details</div>
                                <p style="font-size: .7rem" class=" mb-0 text-text-uppercase"><?= $this->config->item('address_of_company') ?>,<br /> <?= $this->config->item('city') ?>, <?= $this->config->item('pincode') ?></p>
                            </div>
                            <div>
                                <p style="font-size: .7rem" class=" mt-0 mb-0 text-text-uppercase">Email: smrdistributor2255@gmail.com,<br /> Mobile: 9600336339<br>GST Tin No.: <?= $this->config->item('gst_tin_no') ?></p>
                            </div>
                        </div>
                        <div class="text-left border-right border-dark pr-3 pl-1" style="font-size: .7rem">
                            <div class="h6 mb-0">Buyer Details</div>
                            <span style="font-weight: bold" class="p_companyName"></span><br>
                            <span class="p_address"></span><br>
                            <span><span class="p_city"></span>, <span class="p_pincode"></span><br> Mobile:<span class="p_mobile"></span><br><span>GST Tin No.: <span class="p_gstNo"></span></span></span>
                            <!-- <div style="font-size: .7rem" class="">VASANTHAM SUPER MARKET(MDU)</div>
                <div style="font-size: .7rem" class="">Stree of the buyer, mobile , district, etc..,</div>
                <div style="font-size: .7rem" class="">GST Tin NO.: KKJASD9FA9FU43009340J40909</div> -->
                        </div>
                        <!--  <div class="col">
            <div>Tax Invoice</div><br>
            <div>Invoice No.: 9989</div>
            <br><div>Invoice Date: 29/09/1999</div></div> -->
                        <div class="col align-right text-right float-right mr-1">
                            <br>
                            <div class="h4">Tax Invoice</div>
                            <div style="font-size: .7rem">Invoice No.: <span class="invoiceNumber"></span></div>
                            <div style="font-size: .7rem">Invoice Date: <span class="invoiceDate"></span></div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr class="border-dark border-bottom">
                <th style="width: 3vw;" class="border-dark border-left">SNO</th>
                <th style="width: 42vw;" class="border-dark border-left">Particulars</th>
                <th style="width: 8vw;" class="border-dark border-left">HSNCode</th>
                <th style="width: 5vw;" class="border-dark border-left">MRP</th>
                <th style="width: 5vw;" class="border-dark border-left">Rate</th>
                <th style="width: 6vw;" class="border-dark border-left">Qty</th>
                <th style="width: 6vw;" class="border-dark border-left">Free</th>
                <th style="width: 7vw;" class="border-dark border-left">Dis</th>
                <!-- <th style="width: 8vw;" class="border-dark border-left">R.Mar</th> -->
                <th style="width: 5vw;" class="border-dark border-left">GST</th>
                <th style="width: 5vw;" class="border-dark border-left">NETRate</th>
                <th style="width: 8vw;" class="border-dark border-left border-right">Amount</th>
            </tr>
        </thead>
        <tbody class="scrollContent border-dark border-bottom report-container" id="allItemsPrint">
        </tbody>
        <tfoot>
            <tr>
                <td colspan="8" rowspan="5">

                    <table align="center" class="mt-1" border="1" width="90%" cellpadding="1">
                        <thead style="text-align: center;">
                            <tr>
                                <th rowspan="2">GST</th>
                                <th rowspan="2">Taxable Value</th>
                                <th colspan="2">Central Tax</th>
                                <th colspan="2">State Tax</th>
                                <th rowspan="2">Total Tax Amount</th>
                            </tr>
                            <tr>
                                <th>Rate</th>
                                <th>Amount</th>
                                <th>Rate</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody class="gstLists">
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th class="taxableAmount text-right"></th>
                                <th></th>
                                <th class="cgst text-right"></th>
                                <th></th>
                                <th class="cgst text-right"></th>
                                <th class="totalTax text-right"></th>
                            </tr>
                        </tfoot>
                        <tfoot></tfoot>
                    </table>
                    <p class="font-weight-bold text-left mb-0">In words: <span id="amtInWords"></span></p>

                </td>
                <td class="border-dark border-bottom border-left" colspan="2">Taxable amount</td>
                <td class="border-dark border-bottom border-left border-right"><span class="float-right taxableAmount"></span></td>
            </tr>
            <tr>
                <td class="border-dark border-bottom border-left" colspan="2">CGST</td>
                <td class="border-dark border-bottom border-left border-right"><span class="float-right cgst"></span></td>
            </tr>
            <tr>
                <td class="border-dark border-bottom border-left" colspan="2">SGST</td>
                <td class="border-dark border-bottom border-left border-right"><span class="float-right sgst"></span></td>
            </tr>
            <tr>
                <td class="border-dark border-bottom border-left" colspan="2">ROUNDING OFF</td>
                <td class="border-dark border-bottom border-left border-right"><span class="float-right rounding"></span></td>
            </tr>
            <tr>
                <td colspan="2" class="border-dark border-left">Total</td>
                <td class="border-dark border-left border-right"><span class="float-right total"></span></td>
            </tr>
            <tr class="border-top border-dark">
                <td colspan="11">

                    <div class="row ">
                        <div class="col text-left ">
                            <span class="font-weight-bold">Bank Details</span><br>
                            <span>Tamilnadu Mercantile Bank(TMB)</span><br>
                            <span>AC.NO.: 301150050800255</span><br>
                            <span>IFSC Code: TMBL0000301</span><br>
                            <span>Branch: Chikkandar Chavadi, Madurai</span>
                        </div>
                        <div class="col text-right">
                            <p class="font-weight-bold">For SMR DISTRIBUTOR,</p>
                            <p class="mt-5 mb-0">Authorized Signatory</p>
                        </div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content" id="blur" style="font-size: .8rem;">
            <div class="modal-body" id="modalPrint">

            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                <button id="addCollectionSubmit" onclick="printBills()" class="btn btn-primary">Print</button>
            </div>
        </div>

    </div>
</div>

<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script>
    $('#printBtn .loading').hide()
    var base_url = $('#base').val();
    let customerDetails, currentItem, itemsCount = 1,
        allItems = [],
        gstLists = [],
        invoiceNumber = 0,
        invoiceDate, invNoValidated, lastTotal;
    $(document).ready(function() {
        autocomplete({
            input: document.getElementById("product"),
            emptyMsg: "No items found",
            minLength: 3,
            fetch: function(text, update) {
                text = text.toLowerCase();
                var request = $.ajax({
                    url: base_url + 'dashboard/getStacksByText',
                    type: "POST",
                    data: {
                        text: text
                    },
                    success: function(response) {
                        stacks = JSON.parse(response);
                        let results = []
                        let listOfId = allItems.map((bill) => {
                            return bill.id
                        })
                        stacks.forEach((stack) => {
                            if (listOfId.includes(stack.id)) {
                                return
                            } else {
                                results.push({
                                    label: stack.product + '=>(' + stack.stack + ')(' + stack.freeStack + ')',
                                    value: stack.id,
                                    ...stack
                                });
                            }
                        })
                        update(results);
                    }
                });
                // let results = []
                //     // let listOfId = purchaseBills.map((bill) => {
                //     //   return bill.id
                //     // })
                // particulars.forEach((stack) => {
                //     //   if (listOfId.includes(stack.id)) {
                //     //     return
                //     //   } else {
                //     results.push({
                //         label: stack.product + '=>(' + stack.stack + ')(' + stack.freeStack + ')',
                //         value: stack.id,
                //         ...stack
                //     });
                //     //   }
                // })
                // update(results);
            },
            debounceWaitMs: 300,
            enableSubmit: false,
            preventSubmit: false,
            onSelect: function(item) {
                fillCurrentItem(item)
            }
        });
        autocomplete({
            input: document.getElementById("customerDetails"),
            emptyMsg: "No items found",
            minLength: 3,
            fetch: function(text, update) {
                text = text.toLowerCase();
                var request = $.ajax({
                    url: base_url + 'dashboard/getCustomersByText',
                    type: "POST",
                    data: {
                        text: text
                    },
                    success: function(response) {
                        customers = JSON.parse(response);
                        let results = []
                        customers.forEach((customer) => {
                            results.push({
                                label: customer.name + '=>(' + customer.mobile + ')',
                                value: customer.id,
                                ...customer
                            });
                        })
                        update(results);
                    }
                });

                // let results = []
                // customers.forEach((customer) => {
                //     results.push({
                //         label: customer.name + '=>(' + customer.mobile + ')',
                //         value: customer.id,
                //         ...customer
                //     });
                // })
                // update(results);
            },
            debounceWaitMs: 300,
            enableSubmit: false,
            preventSubmit: false,
            onSelect: function(item) {
                customerDetails = item;
                $('#customerDetails').val(item.name);
                $('.p_companyName').empty().text(customerDetails.name);
                $('.p_mobile').empty().text(customerDetails.mobile);
                $('.p_pincode').empty().text(customerDetails.pincode);
                $('.p_gstNo').empty().text(customerDetails.gstNo);
                $('.p_city').empty().text(customerDetails.city);
                $('.p_address').empty().text(customerDetails.address);

            }
        });

        $('#quantity').keyup(function() {
            let quantity = fixNumber($('#quantity').val(), 0)
            const {
                stack
            } = currentItem
            if (quantity < 0) {
                $('#quantity').val(0)
            } else if (isFloat(quantity)) {
                $('#quantity').val(quantity.toFixed())
            } else {
                if (quantity > stack) {
                    $('#quantity').val(0);
                    alert('Quantity must less than or equal to STACK(' + stack + ')')
                    $('#quantity').focus();
                } else {
                    currentItem.quantity = quantity
                    calculateCurrentItem();
                }
            }
        });

        $('#free').keyup(function() {
            const {
                freeStack: stack
            } = currentItem
            let free = fixNumber($('#free').val(), 0)
            free > 0 ? $('#free').val(free) : $('#free').val(0)

            if (free > stack) {
                $('#free').val(0);
                alert('Free must less than or equal to STACK(' + stack + ')')
                $('#free').focus();
            } else {
                currentItem.free = free
            }
        });

        $('#discount').keyup(function() {
            let quantity = fixNumber($('#discount').val(), 0)
            if (quantity < 0) {
                $('#discount').val(0)
            } else if (isFloat(quantity)) {
                $('#discount').val(quantity.toFixed())
            } else {
                currentItem.discount = quantity
                calculateCurrentItem();
            }
        });

        $('#retailerMargin').keyup(function() {
            let quantity = fixNumber($('#retailerMargin').val(), 0)
            if (quantity < 0) {
                $('#retailerMargin').val(0)
            } else if (isFloat(quantity)) {
                $('#retailerMargin').val(quantity.toFixed())
            } else {
                currentItem.retailerMargin = quantity
                calculateCurrentItem();
            }
        });

        $('#billtick').click(function() {
            billEntry();
        })

        $('#billx').click(function() {
            clearCurrentItem();
        })

        $('#printBtn').click(() => {
            // if (!invoiceNumber) {
            //     print()
            //     return false
            // }
            $('#printBtn .loading').show()
            $('#printBtn').prop('disabled', !0);
            var request = $.ajax({
                url: base_url + 'dashboard/checkInvoiceNumber',
                type: "POST",
                success: function(res) {
                    let result = JSON.parse(res);
                    invNoValidated = result.status === 200 ? !0 : 0
                    invoiceNumber = result.invoiceNo < 1000 ? "0"+result.invoiceNo : result.invoiceNo
                    $('#printBtn').prop('disabled', 0);
                    $('#printBtn .loading').hide()
                    print()
                },
            });

            request.fail(function(jqXHR, textStatus) {
                $('#printBtn .loading').hide()
                $('#printBtn').prop('disabled', 0);
                print()
            });
        })

        // $('#invoiceNumber').keyup(() => {
        //     invoiceNumber = $('#invoiceNumber').val().trim()
        //     $('.invoiceNumber').empty().text(invoiceNumber)

        // })

        $('#invoiceDate').change(() => {
            invoiceDate = $('#invoiceDate').val()
            var d = new Date(invoiceDate),
                month = d.getMonth() + 1,
                day = '' + d.getDate(),
                year = d.getFullYear();
            $('.invoiceDate').empty().text(`${day}/${month}/${year}`);
        })

        $('#clearAllBtn').click(() => {
            cancelAll()
        })
    });

    //helper functions

    const isFloat = n => {
        return Number(n) === n && n % 1 !== 0;
    }

    const convertPercentage = i => i / 100

    const checkNum = num => Number(num && num || 0)

    const fixNumber = (v, isFix) => isFix ? checkNum(v).toFixed(2) : Number(v && v || 0)

    const calculateBasicRate = (mrp, retailerMargin, gst) => ((mrp - mrp * convertPercentage(retailerMargin)) / (1 + convertPercentage(gst))).toFixed(2)

    //for current items

    const fillCurrentItem = Object => {
        let {
            gst,
            retailerMargin,
            basicRate,
            mrp,
            taxedRate,
        } = Object
        basicRate = calculateBasicRate(mrp, retailerMargin, gst)
        currentItem = Object
        currentItem.basicRate = fixNumber(basicRate, !0)
        currentItem.taxedRate = fixNumber(taxedRate, !0)
        currentItem.quantity = 0
        currentItem.free = 0
        currentItem.discount = 0
        currentItem.retailerMargin = fixNumber(retailerMargin, 0)
        calculateCurrentItem()
    }

    const calculateCurrentItem = () => {
        let {
            mrp,
            gst,
            quantity,
            discount,
            retailerMargin
        } = currentItem
        //calculate basicrate
        let basicRate = calculateBasicRate(mrp, retailerMargin, gst)
        //calculate discount for single qty
        let amount = basicRate - basicRate * convertPercentage(discount)
        //calculate taxedrate
        let taxedRate = amount + (amount * convertPercentage(gst))
        // calculate total for qty entered without tax
        amount = amount * quantity
        currentItem.quantity = fixNumber(quantity, 0)
        currentItem.basicRate = fixNumber(basicRate, !0)
        currentItem.discount = fixNumber(discount, 0)
        currentItem.amount = fixNumber(amount, !0)
        currentItem.taxedRate = fixNumber(taxedRate, !0)
        updateCurrentItem()
    }

    const updateCurrentItem = () => {
        // let fr = $('#free').val()
        // currentItem.free = fr >= 0 ? fr : 0
        let {
            gst,
            product,
            retailerMargin,
            basicRate,
            mrp,
            hsnCode,
            quantity,
            discount,
            amount,
            taxedRate,
            free
        } = currentItem
        $('#product').val(product), $('#hsnCode').empty().text(hsnCode), $('#basicRate').empty().text(basicRate), $('#mrp').empty().text(mrp),
            $('#discount').val(discount), $('#amount').empty().text(amount), $('#gst').empty().text(gst), $('#free').val(free),
            $('#taxedRate').empty().text(taxedRate), $('#quantity').val(quantity), $('#retailerMargin').val(retailerMargin);
    }

    //for all Items

    const billEntry = () => {
        if (!currentItem) return false;
        if (currentItem.quantity == 0 && currentItem.free == 0) {
            $('#quantity').focus()
            return false;
        }
        currentItem.unique = itemsCount;
        itemsCount++;
        allItems.push(currentItem);
        clearCurrentItem()
        updateAllItems()
    }

    const clearCurrentItem = () => {
        $('#product').val(''), $('#hsnCode').empty().text(), $('#basicRate').empty().text(), $('#mrp').empty().text(),
            $('#discount').val(0), $('#amount').empty().text(), $('#gst').empty().text(), $('#free').val(0),
            $('#taxedRate').empty().text(), $('#quantity').val(0), $('#retailerMargin').val(0);
        $('#product').focus()
    }

    const updateAllItems = () => {
        $('#allItems').empty()
        allItems.forEach((data, i) => {
            $('#allItems').append(`<tr><td><span class="float-left">${++i}</td><td>${data.product}</td><td>${data.hsnCode}</td><td><span class="float-right">${data.mrp}</span></td><td><span class="float-right">${data.basicRate}</span></td><td><span class="float-right">${data.quantity}</span></td><td><span class="float-right">${data.free}</span></td><td><span class="float-right">${data.discount}%</span></td><td><span class="float-right">${data.retailerMargin}%</span></td><td><span class="float-right">${data.gst}%</span></td><td><span class="float-right">${data.taxedRate}</span></td><td><span class="float-right">${data.amount}</span></td><td><button onclick='editBill(${data.unique})' class='btn btn-info'>✏️</button></td><td><button onclick='deleteBill(${data.unique})' class='btn btn-danger'>X</button></td></tr>`);
        });
        calculateTotal()
        updatePrint()
    }

    const updatePrint = () => {
        $('#allItemsPrint').empty()
        allItems.map((data, i) => {
            $('#allItemsPrint').append(`<tr><td class="border-dark border-left"><span class="float-left">${++i}</td><td  class="border-dark border-left float-left text-left">${data.product}</td><td  class="border-dark border-left">${data.hsnCode}</td><td  class="border-dark border-left"><span class="float-right">${data.mrp}</span></td><td  class="border-dark border-left"><span class="float-right">${data.basicRate}</span></td><td  class="border-dark border-left"><span class="float-right">${data.quantity}</span></td><td  class="border-dark border-left"><span class="float-right">${data.free}</span></td><td  class="border-dark border-left"><span class="float-right">${data.discount}%</span></td><td  class="border-dark border-left"><span class="float-right">${data.gst}%</span></td><td  class="border-dark border-left"><span class="float-right">${data.taxedRate}</span></td><td  class="border-dark border-left border-right"><span class="float-right">${data.amount}</span></td></tr>`);
        });
    }

    const groupBy = (array, key) => {
        return array.reduce((result, obj) => {
            (result[obj[key]] = result[obj[key]] || []).push(obj);
            return result;
        }, {});
    }

    const calculateTotal = async () => {
        let total = allItems.reduce((t, bill) => {
            return parseFloat(bill.amount) + parseFloat(t)
        }, 0)
        total = fixNumber(total, !0)
        let temp = groupBy(allItems, 'gst');
        let keyLists = Object.keys(temp)
        let list = [];
        keyLists.map((gst) => {
            let taxableAmount = temp[gst].reduce((t, bill) => {
                return parseFloat(bill.amount) + parseFloat(t)
            }, 0)
            taxableAmount = fixNumber(taxableAmount, !0)
            let cgst = taxableAmount * ((gst / 2) / 100);
            cgst = fixNumber(cgst, !0)
            list.push({
                taxableAmount: taxableAmount,
                gst: gst,
                cgst: cgst,
                totalTax: cgst + cgst
            })
            gstLists = list;
        })
        let cgst = gstLists.reduce((t, {
            totalTax
        }) => {
            return parseFloat(totalTax) + parseFloat(t)
        }, 0)
        cgst = fixNumber(cgst, !0)
        $('.cgst').empty().text(cgst)
        $('.sgst').empty().text(cgst)
        $('.totalTax').empty().text((+cgst + +cgst).toFixed(2))
        let finalTotal = Number(total) + Number(cgst) + Number(cgst)
        finalTotal = fixNumber(finalTotal, !0)
        $('.taxableAmount').empty().text(total)
        let withDecimal = finalTotal
        let withOutDecimal = Number(finalTotal).toFixed()
        let decimal = withOutDecimal - withDecimal
        decimal = parseFloat(decimal.toPrecision(2))
        finalTotal = Number(finalTotal).toFixed()
        lastTotal = Number(finalTotal).toFixed(2)
        $('.total').empty().text(Number(finalTotal).toFixed(2))
        $('.rounding').empty().text(Number(decimal).toFixed(2))
        fillGstList()
        $('#amtInWords').empty().text(numberToEnglish(finalTotal).toUpperCase() + ' ONLY');
    }

    const fillGstList = () => {
        $('.gstLists').empty()
        gstLists.map((data) => {
            let totalTax = Number(data.cgst) + Number(data.cgst)
            totalTax = fixNumber(totalTax, !0)
            $('.gstLists').append(`<tr><td class="text-right">GST ${data.gst}%</td><td class="text-right">${data.taxableAmount}</td><td class="text-right">${data.gst / 2}%</td><td class="text-right">${data.cgst}</td><td class="text-right">${data.gst / 2}%</td><td class="text-right">${data.cgst}</td><td class="text-right">${totalTax}</td></tr>`);
        });
    }

    const editBill = unique => {
        currentItem = Object.assign({}, getBill(unique))
        updateCurrentItem();
        deleteBill(unique);
    }


    const deleteBill = unique => {
        allItems = allItems.filter((bill) => {
            return bill.unique != unique
        });
        updateAllItems();
        $('#product').focus();
    }


    function getBill(unique) {
        object = allItems.filter((bill) => bill.unique === unique)
        let res = Object.assign({}, object[0])
        return res;
    }

    const print = () => {
        if (allItems.length === 0) {
            return false
        }
            $('.invoiceNumber').empty().text(invoiceNumber)
        // if (!invoiceNumber || invoiceNumber.length != 4) {
        //     alert('Please enter invoice number in 4 digit...')
        //     return false
        // }
        // checkInvoiceNumber(invoiceNumber)
        invoiceDate = $('#invoiceDate').val()

        if (!customerDetails) {
            alert('Please enter Customer name...')
            return false
        }
        if (!invoiceDate || invoiceDate === '') {
            alert('Please fill Invoice Date');
            return false;
        }


        if (!invNoValidated) {
            alert('Invoice number already exists...')
            return false
        }
        let printForm = $('#check').html();
        $('#modalPrint').empty().append(printForm)

        $('.bd-example-modal-xl').modal('show');

    }

    function checkInvoiceNumber(invoiceNumber) {
        var request = $.ajax({
            url: base_url + 'dashboard/checkInvoiceNumber',
            type: "POST",
            data: {
                invoiceNumber
            },
            success: function(res) {
                let status = JSON.parse(res)
                invNoValidated = !status.status
            },
        });
    }



    function printBills() {
        let printForm = $('#check').html();
        $("#blur").addClass("blur");
        $('body').scrollTop(0);



        var opt = {
            margin: .3,
            filename: invoiceNumber + '.pdf',
            image: {
                type: 'jpeg',
                // quality: 0.98
                quality: 1
            },
            html2canvas: {
                scale: 1.5,
                logging:true
            },
            jsPDF: {
                unit: 'in',
                format: 'a4',
                orientation: 'portrait'
            }
        }

        var request = $.ajax({
            url: base_url + 'dashboard/addBill',
            type: "POST",
            data: {
                bills: allItems,
                total: lastTotal,
                customer: customerDetails.id,
                invoiceNumber: invoiceNumber,
                invoiceDate: $('#invoiceDate').val(),
            },
            success: function(response) {
                setTimeout(function() {

                    $("#blur").removeClass("blur");
                    $('.bd-example-modal-xl').modal('hide');
                    $('body').scrollTop(0);
                    html2pdf().set(opt).from(printForm).outputPdf('datauristring').then((res) => {
                        sendPDF(res)
                        var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=2050,height=1050';

                        var htmlPop = `<embed width=100% height=100% type="application/pdf" src="${res}"></embed>`
                        var printWindow = window.open("", "PDF", winparams);
                        printWindow.document.write(htmlPop);
                        setTimeout(function() {
                            printWindow.close()
                        }, 20000);
                    });
                    $('.allItemsPrint').empty()
                }, 3000);
            },
        });



        request.fail(function(jqXHR, textStatus) {
            $("#blur").removeClass("blur");
            $('.bd-example-modal-xl').modal('hide');
            alert("Request failed: error");
        });
    }

    function sendPDF(data) {
        var request = $.ajax({
            url: base_url + 'dashboard/do_upload',
            type: "POST",
            data: {
                pdf: data,
                invoiceNumber: invoiceNumber,
            },
            success: function(response) {
                cancelAll();
            },
        });



        request.fail(function(jqXHR, textStatus) {
            alert("Sorry pdf not saved");
        });
    }

    const cancelAll = () => {
        clearCurrentItem()
        customerDetails = null
        $('#customerDetails').val('')
        invoiceNumber = null
        // $('#invoiceNumber').val('')
        allItems = []
        updateAllItems()
    }


    function numberToEnglish(n, custom_join_character) {

        var string = n.toString(),
            units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words;

        var and = custom_join_character || 'and';

        /* Is number zero? */
        if (parseInt(string) === 0) {
            return 'zero';
        }

        /* Array of units as words */
        units = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

        /* Array of tens as words */
        tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

        /* Array of scales as words */
        scales = ['', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion'];

        /* Split user arguemnt into 3 digit chunks from right to left */
        start = string.length;
        chunks = [];
        while (start > 0) {
            end = start;
            chunks.push(string.slice((start = Math.max(0, start - 3)), end));
        }

        /* Check if function has enough scale words to be able to stringify the user argument */
        chunksLen = chunks.length;
        if (chunksLen > scales.length) {
            return '';
        }

        /* Stringify each integer in each chunk */
        words = [];
        for (i = 0; i < chunksLen; i++) {

            chunk = parseInt(chunks[i]);

            if (chunk) {

                /* Split chunk into array of individual integers */
                ints = chunks[i].split('').reverse().map(parseFloat);

                /* If tens integer is 1, i.e. 10, then add 10 to units integer */
                if (ints[1] === 1) {
                    ints[0] += 10;
                }

                /* Add scale word if chunk is not zero and array item exists */
                if ((word = scales[i])) {
                    words.push(word);
                }

                /* Add unit word if array item exists */
                if ((word = units[ints[0]])) {
                    words.push(word);
                }

                /* Add tens word if array item exists */
                if ((word = tens[ints[1]])) {
                    words.push(word);
                }

                /* Add 'and' string after units or tens integer if: */
                if (ints[0] || ints[1]) {

                    /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
                    if (ints[2] || !i && chunksLen) {
                        words.push(and);
                    }

                }

                /* Add hundreds word if array item exists */
                if ((word = units[ints[2]])) {
                    words.push(word + ' hundred');
                }

            }

        }

        return words.reverse().join(' ');

    }
</script>