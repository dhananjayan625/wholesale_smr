          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="#" data-toggle="modal" data-target="#addCompanyModal" class="btn btn-primary btn-icon-split btn-sm float-left">
                <span class="icon text-white-50">
                  <i class="fas fa-plus"></i>
                </span>
                <span class="text">Add Distributor</span>
              </a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="companyTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Distributor's Name</th>
                      <th>GST No.</th>
                      <th>Mobile</th>
                      <th>Mail Id</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
<!-- 
                    <?php $i=1; foreach ($companies as $company) { ?>
                      <tr>
                        <th><?=$i?></th>
                        <th><?=$company->name?></th>
                        <th><?=$company->gstNo?></th>
                        <th><?=$company->mobile?></th>
                        <th><?=$company->mailId?></th>
                        <th><a href="#">Edit</a></th>
                      </tr>
                    <?php $i++; } ?> -->
                  </tbody>
                </table>
              </div>
            </div>
          </div>






    <!-- The Modal -->
<div class="modal" id="addCompanyModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Distributor</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <form name="form" id="addcompany" class="col" method="POST" action="">
      <div class="form-group">
        <label for="name">Company name</label>
        <input autocomplete="off" type="text" required class="form-control" id="name" name="name" placeholder="Enter Company name">
      </div>
      <div class="row">
        <div class="form-group col">
          <label for="gstNo">GST No.</label>
          <input type="text" required class="form-control" id="gstNo" name="gstNo" placeholder="Enter GST No.">
        </div>
        <div class="form-group col">
          <label for="mobile">Mobile</label>
          <input type="text" required class="form-control" id="mobile" name="mobile" placeholder="Enter mobile">
        </div>
      </div>
      <div class="form-group">
        <label for="address">Address</label>
        <input type="text" required class="form-control" id="address" name="address" placeholder="Enter address">
      </div>
      <div class="row">
        <div class="col form-group">
          <label for="city">City</label>
          <input type="text" required class="form-control" id="city" name="city" placeholder="Enter city">
        </div>
        <div class="col form-group">
          <label for="state">State</label>
          <input type="text" required class="form-control" id="state" name="state" placeholder="Enter state">
        </div>
      </div>
      <div class="form-group">
        <label for="mailId">Mail Id</label>
        <input type="text" required class="form-control" id="mailId" name="mailId" placeholder="Enter mailId">
      </div>
      <div class="row">
        <div class="form-group col">
          <label for="pincode">Pincode</label>
          <input type="text" required class="form-control" id="pincode" name="pincode" placeholder="Enter pincode">
        </div>
        <div class="col mt-4">
          <button type="submit" id="resetBtn" class="btn btn-primary">Submit</button>
          <button type="reset" class="btn btn-secondary" id="resetbtn" value="Reset">Reset</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>    
<div class="modal fade companyEi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Distributor</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <form name="form" id="editCcompanyForm" class="col" method="POST" action="">
      <input type="hidden" name="eid" id="eid" >
      <div class="form-group">
        <label for="ename">Company name</label>
        <input autocomplete="off" type="text" required class="form-control" id="ename" name="ename" placeholder="Enter Company name">
      </div>
      <div class="row">
        <div class="form-group col">
          <label for="egstNo">GST No.</label>
          <input type="text" required class="form-control" id="egstNo" name="egstNo" placeholder="Enter GST No.">
        </div>
        <div class="form-group col">
          <label for="emobile">Mobile</label>
          <input type="text" required class="form-control" id="emobile" name="emobile" placeholder="Enter mobile">
        </div>
      </div>
      <div class="form-group">
        <label for="eaddress">Address</label>
        <input type="text" required class="form-control" id="eaddress" name="eaddress" placeholder="Enter address">
      </div>
      <div class="row">
        <div class="col form-group">
          <label for="ecity">City</label>
          <input type="text" required class="form-control" id="ecity" name="ecity" placeholder="Enter city">
        </div>
        <div class="col form-group">
          <label for="estate">State</label>
          <input type="text" required class="form-control" id="estate" name="estate" placeholder="Enter state">
        </div>
      </div>
      <div class="form-group">
        <label for="emailId">Mail Id</label>
        <input type="text" required class="form-control" id="emailId" name="emailId" placeholder="Enter mailId">
      </div>
      <div class="row">
        <div class="form-group col">
          <label for="epincode">Pincode</label>
          <input type="text" required class="form-control" id="epincode" name="epincode" placeholder="Enter pincode">
        </div>
        <div class="col mt-4">
          <button type="submit" class="btn btn-primary">Submit</button>
          <button type="reset" class="btn btn-secondary" id="eresetbtn" value="Reset">Reset</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>
<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script>
const base_url = $('#base').val();
let table;
$("#resetbtn").click(function(){
  resetAddCompanyForm()
});
$("#editCcompanyForm").submit((e) => {
  e.preventDefault();
  updateCompany();
});
$("#addcompany").submit((e) => {
  e.preventDefault();
  addCompany();
});
$(document).ready( function () {
  resetAddCompanyForm()
table = $('#companyTable').DataTable({
  "aLengthMenu": [[10 ,25, 50, 75, -1], [10 ,25, 50, 75, "All"]],
  "iDisplayLength": 10,
        "responsive": true,
 
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": base_url+"dashboard/getAllCompanies",
            "type": "POST",
        },
 
        // "columnDefs": [
        // { 
        //     "targets": [ 0, 2, 3, 5, 7,8 ], 
        //     "orderable": false,
        // },
        // ],
    "fixedHeader": true,
    "info": false,
});
});
const resetAddCompanyForm = () => {
    $('#name').val('');
    $('#gstNo').val('');
    $('#mobile').val('');
    $('#mailId').val('');
    $('#address').val('');
    $('#pincode').val('');
    $('#city').val('');
    $('#state').val('');
  }
const resetEditProductForm = () => {
    $('#ename').val('');
    $('#egstNo').val('');
    $('#emobile').val('');
    $('#emailId').val('');
    $('#eaddress').val('');
    $('#epincode').val('');
    $('#ecity').val('');
    $('#estate').val('');
  }
const fillEditDetails = async ({name, gstNo, mobile, address, pincode, city, state, id, mailId}) => {
  await $('#ename').val(name), $('#egstNo').val(gstNo), $('#emobile').val(mobile), $('#emailId').val(mailId), $('#eaddress').val(address), $('#epincode').val(pincode), $('#ecity').val(city), $('#estate').val(state), $('#eid').val(id);
}
const addCompany = () => {
    let data = {};
    data.name = $('#name').val();
    data.gstNo = $('#gstNo').val();
    data.mobile = $('#mobile').val();
    data.address = $('#address').val();
    data.pincode = $('#pincode').val();
    data.city = $('#city').val();
    data.state = $('#state').val();
    data.id = $('#id').val();
    data.mailId = $('#mailId').val();
   var request = $.ajax({
      url: base_url+'dashboard/addCompany',
      type: "POST",
      data,
      success: async response => {
        await table.ajax.reload();
        await resetAddCompanyForm()
        $('#addCompanyModal').modal('hide');
        alert('Company added successfully')
      }
    });
}
const editCompany = data => {
  fillEditDetails(data)
  $('.companyEi').modal('show')
}
const updateCompany = () => {
    let data = {};
    data.id = $('#eid').val();
    data.name = $('#ename').val();
    data.gstNo = $('#egstNo').val();
    data.mobile = $('#emobile').val();
    data.address = $('#eaddress').val();
    data.pincode = $('#epincode').val();
    data.city = $('#ecity').val();
    data.state = $('#estate').val();
    data.mailId = $('#emailId').val();
   var request = $.ajax({
      url: base_url+'dashboard/updateCompany',
      type: "POST",
      data,
      success: async response => {
        await table.ajax.reload();
        await resetEditProductForm()
        await $('.companyEi').modal('hide');
        alert('Product added successfully')
      }
    });
}
</script>

<input type="hidden" id="base" value="<?php echo base_url(); ?>">


