
<div class="table-responsive mt-5"	>
	<table class="table-striped table-bordered  table" style="font-size: .9rem;" id="stackstable" width="100%" cellspacing="0">
	  <thead class="text-center">
	    <tr>
	    	<th style="width: 7px;">SI.No</th>
	    	<th style="width: 7px;" class="th">Invoice Number</th>
	      <th class="th">Product Name</th>
	      <th style="width: 7px;" class="th">Purchase Price</th>
	      <th style="width: 7px;" class="th">MRP</th>
	      <th style="width: 7px;" class="th">Quantity</th>
	      <th style="width: 7px;" class="th">Stock</th>
	      <th style="width: 7px;" class="th">Free</th>
	      <th style="width: 7px;" class="th">Free Stock</th>
	      <th class="th">Company</th>
	    </tr>
	  </thead>
	  <tfoot>
	    <tr>
	    	<th>Si.No</th>
	    	<th>Invoice Number</th>
	      <th>Product Name</th>
	      <th>Purchase Price</th>
	      <th>MRP</th>
	      <th>Quantity</th>
	      <th>Stack</th>
	      <th>Free</th>
	      <th>Free Stack</th>
	      <th>Company</th>
	    </tr>
	</tfoot>
	  <tbody style="font-weight: 500;">
	  </tbody>
	</table>
</div>
<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script>
	
var base_url = $('#base').val();
$(document).ready( function () {

    $('#stackstable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
    } );

var table = $('#stackstable').DataTable({
	"aLengthMenu": [[10 ,25, 50, 75, -1], [10 ,25, 50, 75, "All"]],
	"iDisplayLength": 10,
        "responsive": true,
 
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": base_url+"dashboard/getStacksDatatable",
            "type": "POST",
        },
 
        "columnDefs": [
        { 
            "targets": [ 0 ], 
            "orderable": false,
        },
        { 
            "targets": [ 3 ],
            "orderable": false,
        },
        { 
            "targets": [ 4 ],
            "orderable": false,
        },
        { 
            "targets": [ 5 ],
            "orderable": false,
        },
        { 
            "targets": [ 6 ],
            "orderable": false,
        },
        { 
            "targets": [ 7 ],
            "orderable": false,
        },
        { 
            "targets": [ 8	 ],
            "orderable": false,
        }

        ],
    "fixedHeader": true,
    "info": false,
        "initComplete": function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        }


});
});


</script>