<div class="row mt-4">
	
    <div class="table-responsive mt-4">
      <table class="table table-bordered" id="invoiceTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>Invoice no.</th>
            <th>Invoice date</th>
            <th>Amount</th>
            <th>Paid amount</th>
            <th>Pending amount</th>
            <th>Customer</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

         <!--  <?php $i=1; foreach ($invoices as $invoice) { ?>
            <tr>
              <th><?=$i?></th>
              <th><?=$invoice->invoiceNumber?></th>
              <th><?=$invoice->invoiceDate?></th>
              <th><?=$invoice->amount?></th>
              <th><?=$invoice->paidAmount?></th>
              <th><?=$invoice->pendingAmount?></th>
              <th><a target="_blank" href="<?=base_url()?>assets/uploads/invoice_<?=$invoice->invoiceNumber?>.pdf">View</a></th>
            </tr>
          <?php $i++; } ?> -->
        </tbody>
      </table>
    </div>
</div>

<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script>
const base_url = $('#base').val();
	$(document).ready( function () {
table = $('#invoiceTable').DataTable({

        "responsive": true,
        // "info": false,
  "aLengthMenu": [[10 ,25, 50, 75, -1], [10 ,25, 50, 75, "All"]],
  "iDisplayLength": 10,
        "responsive": true,
 
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": base_url+"dashboard/getAllInvoices",
            "type": "POST",
        },
 
        // "columnDefs": [
        // { 
        //     "targets": [ 0, 2, 3, 5, 7,8 ], 
        //     "orderable": false,
        // },
        // ],
    "fixedHeader": true,
    "info": false,
});
});


</script>