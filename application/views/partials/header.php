<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />

        <link rel="shortcut icon" href="<?=base_url()?>assets/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?=base_url()?>assets/images/favicon.ico" type="image/x-icon">

        <title><?=$title ?>| Dashboard</title>
        <style type="text/css">
            .btn-label {position: relative;left: -8px; display: inline-block;padding: 6px 12px; background: rgba(0,0,0,0.15);border-radius: 3px 0 0 3px;}
.btn-labeled {padding-top: 0;padding-bottom: 0;}
.btn { margin-bottom:10px; }
        </style>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script defer src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
        <script defer src="https://cdn.datatables.net/responsive/2.2.6/js/responsive.bootstrap.min.js"></script>
        <script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js" defer></script>
        <script type="text/javascript" defer src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" defer src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>        
        <script defer src="https://kraaden.github.io/autocomplete/autocomplete.js"></script>
        <script defer src="<?=base_url()?>/assets/template/dist/js/scripts.js"></script>


        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap.min.css">
        <link href="<?=base_url()?>/assets/template/dist/css/styles.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="https://kraaden.github.io/autocomplete/autocomplete.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />   
        <link rel="stylesheet" type="text/css" href="<?=base_url('assets/template/dist/css/styles.css')?>">
        <style type="text/css">

            /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
        </style>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Wholesale</a>
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <!-- <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form> -->
            <!-- Navbar-->
            <ul class="navbar-navd-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <!-- <a class="dropdown-item" href="#">Settings</a>
                        <a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div> -->
                        <a class="dropdown-item" href="<?=base_url('dashboard/logout')?>">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu notPrintDiv">
                        <div class="nav">
                            <a class="nav-link" href="<?=base_url()?>dashboard">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/companies">
                                <div class="sb-nav-link-icon"><i class="fas fa-building"></i></div>
                                Distributor
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/product">
                                <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                                Product
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/purchase">
                                <div class="sb-nav-link-icon"><i class="fas fa-arrow-down"></i></div>
                                Purchase
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/stack">
                                <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                                Stock
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/customers">
                                <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                                Customer
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/billing">
                                <div class="sb-nav-link-icon"><i class="fas fa-arrow-up"></i></div>
                                Billing
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/Invoice">
                                <div class="sb-nav-link-icon"><i class="fas fa-file"></i></div>
                                Invoice
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/collections">
                                <div class="sb-nav-link-icon"><i class="fas fa-share-alt"></i></div>
                                Collections
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/purchaseReturns">
                                <div class="sb-nav-link-icon"><i class="fas fa-file"></i></div>
                                Purchase returns
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/purchaseReport">
                                <div class="sb-nav-link-icon"><i class="fas fa-file"></i></div>
                                Purchase Report
                            </a>
                            <a class="nav-link" href="<?=base_url()?>dashboard/invoiceReport">
                                <div class="sb-nav-link-icon"><i class="fas fa-file"></i></div>
                                Invoice Report
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer notPrintDiv">
                        <div class="small">Logged in as:</div>
                        Admin
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">