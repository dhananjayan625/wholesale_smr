
                        <h1 class="mt-4">Dashboard</h1>
                        <!-- <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol> -->
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body"><span class="h5">Total Stock Amount:</span><br><span class="h1"><span>Rs. </span><span id="totalStackAmount"></span></span><br><span id="totalStackAmountInWords" class=" font-weight-normal"></span></div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body"><span class="h5">Total Collection Pending:</span><br><span class="h1"><span>Rs. </span><span id="totalCollectionPendingAmount"></span></span><br><span id="totalCollectionPendingAmountInWords" class=" font-weight-normal"></span></div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body"><span class="h5">Purchase Return Pending:</span><br><span class="h1"><span>Rs. </span><span id="totalPurchaseReturnAmount"></span></span><br><span id="totalPurchaseReturnAmountInWords" class=" font-weight-normal"></span></div>
                                </div>
                            </div>
                        </div>

<script>
    var totalStackAmount = parseInt(<?=$total_stack_amount?>)
    var totalCollectionPendingAmount = parseInt(<?=$total_collection_pending_amount?>)
    var totalPurchaseReturnAmount = parseInt(<?=$total_purchase_return_amount?>)
    $('#totalStackAmount').text(totalStackAmount)
    var totalStackAmountInWords = numberToEnglish(totalStackAmount).toUpperCase()
    $('#totalStackAmountInWords').text('( '+totalStackAmountInWords+' )')
    $('#totalCollectionPendingAmount').text(totalCollectionPendingAmount)
    var totalCollectionPendingAmountInWords = numberToEnglish(totalCollectionPendingAmount).toUpperCase()
    $('#totalCollectionPendingAmountInWords').text('( '+totalCollectionPendingAmountInWords+' )')
    $('#totalPurchaseReturnAmount').text(totalPurchaseReturnAmount)
    var totalPurchaseReturnAmountInWords = numberToEnglish(totalPurchaseReturnAmount).toUpperCase()
    $('#totalPurchaseReturnAmountInWords').text('( '+totalPurchaseReturnAmountInWords+' )')
function numberToEnglish(n, custom_join_character) {

    var string = n.toString(),
        units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words;

    var and = custom_join_character || 'and';

    /* Is number zero? */
    if (parseInt(string) === 0) {
        return 'zero';
    }

    /* Array of units as words */
    units = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

    /* Array of tens as words */
    tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

    /* Array of scales as words */
    scales = ['', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion'];

    /* Split user arguemnt into 3 digit chunks from right to left */
    start = string.length;
    chunks = [];
    while (start > 0) {
        end = start;
        chunks.push(string.slice((start = Math.max(0, start - 3)), end));
    }

    /* Check if function has enough scale words to be able to stringify the user argument */
    chunksLen = chunks.length;
    if (chunksLen > scales.length) {
        return '';
    }

    /* Stringify each integer in each chunk */
    words = [];
    for (i = 0; i < chunksLen; i++) {

        chunk = parseInt(chunks[i]);

        if (chunk) {

            /* Split chunk into array of individual integers */
            ints = chunks[i].split('').reverse().map(parseFloat);

            /* If tens integer is 1, i.e. 10, then add 10 to units integer */
            if (ints[1] === 1) {
                ints[0] += 10;
            }

            /* Add scale word if chunk is not zero and array item exists */
            if ((word = scales[i])) {
                words.push(word);
            }

            /* Add unit word if array item exists */
            if ((word = units[ints[0]])) {
                words.push(word);
            }

            /* Add tens word if array item exists */
            if ((word = tens[ints[1]])) {
                words.push(word);
            }

            /* Add 'and' string after units or tens integer if: */
            if (ints[0] || ints[1]) {

                /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
                if (ints[2] || !i && chunksLen) {
                    words.push(and);
                }

            }

            /* Add hundreds word if array item exists */
            if ((word = units[ints[2]])) {
                words.push(word + ' hundred');
            }

        }

    }

    return words.reverse().join(' ');

}
</script>