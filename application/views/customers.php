<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="table-responsive mt-4">
      <table class="table table-bordered" id="productTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>GST No.</th>
            <th>Mobile</th>
            <th>Mail Id</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

          <!-- <?php $i=1; foreach ($customers as $customer) { ?>
            <tr>
              <th><?=$i?></th>
              <th><?=$customer->name?></th>
              <th><?=$customer->gstNo?></th>
              <th><?=$customer->mobile?></th>
              <th><?=$customer->mailId?></th>
              <th><a href='#' onclick='editCustomer(<?=json_encode($customer)?>)'>Edit</a></th>
            </tr>
          <?php $i++; } ?> -->
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-md-4"> 
    <h3 class="py-3">Add Customer</h3>
    <form name="form" id="form" method="POST" action="">
      <input type="hidden" name="id" id="id">
      <div class="form-group">
        <label for="name">Customer name</label>
        <input autocomplete="off" required type="text" class="form-control" id="name" name="name" placeholder="Enter Customer name">
      </div>
      <div class="row">
        <div class="form-group col">
          <label for="gstNo">GST No.</label>
          <input type="text" required class="form-control" id="gstNo" name="gstNo" placeholder="Enter GST No.">
        </div>
        <div class="form-group">
          <label for="mobile">mobile</label>
          <input type="text" required class="form-control" id="mobile" name="mobile" placeholder="Enter mobile">
        </div>
      </div>
      <div class="form-group">
        <label for="address">address</label>
        <input type="text" required class="form-control" id="address" name="address" placeholder="Enter address">
      </div>
      <div class="row">
        <div class="col form-group">
          <label for="city">city</label>
          <input type="text" required class="form-control" id="city" name="city" placeholder="Enter city">
        </div>
        <div class="col form-group">
          <label for="state">state</label>
          <input type="text" required class="form-control" id="state" name="state" placeholder="Enter state">
        </div>
      </div>
      <div class="form-group">
        <label for="mailId">mailId</label>
        <input type="text" class="form-control" id="mailId" name="mailId" placeholder="Enter mailId">
      </div>
      <div class="row">
        <div class="form-group col">
          <label for="pincode">pincode</label>
          <input type="text" required class="form-control" id="pincode" name="pincode" placeholder="Enter pincode">
        </div>
        <div class="col mt-4">
          <span id="submitBtn"></span>
          <button type="button" class="btn btn-secondary" id="resetbtn" value="Reset">Reset</button>
        </div>
      </div>
    </form>
  </div>
</div>
<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script>
const base_url = $('#base').val();
  var isUpdateForm = 0, table;
  const updateSubmitBtn = async () => await $('#submitBtn').empty().append(isUpdateForm ? `<button type="submit" id="resetBtn" class="btn btn-primary">Update</button>` : `<button type="submit" id="resetBtn" class="btn btn-primary">Submit</button>`)
    updateSubmitBtn()
  $("#resetbtn").click(function(){
    isUpdateForm = 0;
    reserForm()
});
  async function reserForm() {
    updateSubmitBtn()
    await $('#name').val(''), $('#mobile').val(''), $('#address').val(''), $('#city').val(''), $('#state').val(''), $('#mailId').val(''), $('#pincode').val(''), $('#gstNo').val('');
  }
  async function editCustomer({id, name, mobile, address, city, state, mailId, pincode, gstNo}) {
    isUpdateForm = !0
    updateSubmitBtn()
    await $('#id').val(id), $('#name').val(name), $('#mobile').val(mobile), $('#address').val(address), $('#city').val(city), $('#state').val(state), $('#mailId').val(mailId), $('#pincode').val(pincode), $('#gstNo').val(gstNo);
  }
  function addCustomer() {

      var request = $.ajax({
        url: base_url+'dashboard/addCustomer',
        type: "POST",
        data: {
          name: $('#name').val(), 
          mobile: $('#mobile').val(), 
          address: $('#address').val(), 
          city: $('#city').val(), 
          state: $('#state').val(), 
          mailId: $('#mailId').val(), 
          pincode: $('#pincode').val(), 
          gstNo: $('#gstNo').val()
        },
        success: function(response) {
    isUpdateForm = 0;
    reserForm()
        },
      });



        request.fail(function(jqXHR, textStatus) {
          $('.notPrintDiv').css("display","block");
          $('.printdivBody').empty()
          $('.printdiv').hide()
          alert( "Request failed: " + textStatus );
        });
  }
  function updateCustomer() {

      var request = $.ajax({
        url: base_url+'dashboard/updateCustomer',
        type: "POST",
        data: {
          id: $('#id').val(),
          name: $('#name').val(), 
          mobile: $('#mobile').val(), 
          address: $('#address').val(), 
          city: $('#city').val(), 
          state: $('#state').val(), 
          mailId: $('#mailId').val(), 
          pincode: $('#pincode').val(), 
          gstNo: $('#gstNo').val()
        },
        success: function(response) {
    isUpdateForm = 0;
    reserForm()
        },
      });



        request.fail(function(jqXHR, textStatus) {
          $('.notPrintDiv').css("display","block");
          $('.printdivBody').empty()
          $('.printdiv').hide()
          alert( "Request failed: " + textStatus );
        });
  }
$(document).ready( function () {


table = $('#productTable').DataTable({

        "responsive": true,
        // "info": false,
  "aLengthMenu": [[10 ,25, 50, 75, -1], [10 ,25, 50, 75, "All"]],
  "iDisplayLength": 10,
        "responsive": true,
 
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": base_url+"dashboard/getAllCustomers",
            "type": "POST",
        },
 
        // "columnDefs": [
        // { 
        //     "targets": [ 0, 2, 3, 5, 7,8 ], 
        //     "orderable": false,
        // },
        // ],
    "fixedHeader": true,
    "info": false,
});


$("#form").submit(e => {
  e.preventDefault()
  isUpdateForm ? updateCustomer() : addCustomer()
  table.ajax.reload();
});
});
</script>